package com.innovenso.townplan.writer.svg

import com.innovenso.townplan.api.value.aspects.ContentOutputType
import com.innovenso.townplan.api.value.aspects.FatherTimeType
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.domain.sample.SampleFactory
import com.innovenso.townplan.repository.AssetRepository
import com.innovenso.townplan.repository.FileSystemAssetRepository
import com.innovenso.townplan.writer.plantuml.TownPlanPlantUMLHighlevelItPlatformDiagramWriter
import org.apache.commons.io.FileUtils
import spock.lang.Specification

import java.time.LocalDate

class PlatformDiagramWriterSpec extends Specification {
	private TownPlanImpl townPlan = new TownPlanImpl()
	private SampleFactory samples = new SampleFactory(townPlan)
	private static File targetDirectory = FileUtils.getTempDirectory()
	private static File assetDirectory = new File(targetDirectory, "assets")
	private static AssetRepository assetRepository = new FileSystemAssetRepository(assetDirectory.getAbsolutePath(), "http://localhost:8080/files")

	def "time machine aware platform diagrams"() {
		given:
		def enterprise = samples.enterprise()
		def platform = samples.platform("platform", "The Platform")
		def website = samples.system("central", "Customer Website", platform.key)
		def sso = samples.system("sso", "CIAM", platform.key)
		def cmp = samples.system("cmp", "Consent Management Platform")
		def datalake = samples.system("datalake", "Data lake")
		def esb = samples.system("esb", "Enterprise Service Bus", platform.key)
		def dwh = samples.system("dwh", "Data Warehouse", platform.key)
		def user = samples.actor(enterprise)
		def admin = samples.actor(enterprise)
		samples.flow(user, website)
		samples.flow(website, sso)
		samples.flow(website, cmp)
		samples.flow(website, esb)
		samples.flow(esb, dwh)
		samples.flow(sso, datalake)
		samples.flow(cmp, datalake)
		samples.flow(website, datalake)
		samples.flow(user, sso)
		samples.flow(user, cmp)
		def one = LocalDate.of(2020,7,1)
		def two = LocalDate.of(2021,7,1)
		def three = LocalDate.of(2022,7,1)
		def a = LocalDate.of(2020, 8, 1)
		def b = LocalDate.of(2021, 8, 1)
		def pointOne = samples.keyPointInTime(one)
		def pointTwo = samples.keyPointInTime(two)
		def pointThree = samples.keyPointInTime(three)
		samples.fatherTime(datalake, a, FatherTimeType.ACTIVE)
		samples.fatherTime(cmp, a, FatherTimeType.ACTIVE)
		samples.fatherTime(cmp, a, FatherTimeType.ACTIVE)
		samples.fatherTime(esb, b, FatherTimeType.DEATH)
		samples.fatherTime(esb, a, FatherTimeType.RETIRED)
		samples.fatherTime(sso, a, FatherTimeType.DEATH)
		TownPlanPlantUMLHighlevelItPlatformDiagramWriter writer = new TownPlanPlantUMLHighlevelItPlatformDiagramWriter(targetDirectory, assetRepository, "timemachine", ContentOutputType.SVG)
		def modelOne = writer.getModel(platform, pointOne, townPlan)
		def modelTwo = writer.getModel(platform, pointTwo, townPlan)
		def modelThree = writer.getModel(platform, pointThree, townPlan)
		List<File> renderedFiles = []
		when:
		writer.write(townPlan, platform, renderedFiles)
		then:
		List.of(pointOne, pointTwo, pointThree).each {
			println esb.getLifecycle(it)
		}
		renderedFiles.size() == 4
		renderedFiles.each { println it.absolutePath }
	}
}
