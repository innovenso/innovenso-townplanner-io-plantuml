package com.innovenso.townplan.writer.svg

import com.innovenso.townplan.api.value.aspects.ContentOutputType
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.domain.sample.SampleFactory
import com.innovenso.townplan.repository.AssetRepository
import com.innovenso.townplan.repository.FileSystemAssetRepository
import com.innovenso.townplan.writer.plantuml.TownPlanPlantUMLItSystemDataEntitiesDiagramWriter
import org.apache.commons.io.FileUtils
import spock.lang.Specification

class SystemEntityRelationshipDiagramWriterSpec extends Specification {
	private TownPlanImpl townPlan = new TownPlanImpl()
	private SampleFactory samples = new SampleFactory(townPlan)
	private static File targetDirectory = FileUtils.getTempDirectory()
	private static File assetDirectory = new File(targetDirectory, "assets")
	private static AssetRepository assetRepository = new FileSystemAssetRepository(assetDirectory.getAbsolutePath(), "http://localhost:8080/files")

	def "system entity relationship diagrams"() {
		given:
		def enterprise = samples.enterprise()
		def system = samples.system()
		def entity1 = samples.entity(enterprise.key)
		def entity2 = samples.entity(enterprise.key)
		def entity3 = samples.entity(enterprise.key)
		def entity4 = samples.entity(enterprise.key)
		def entity5 = samples.entity(enterprise.key)
		samples.composedOf(system,entity1)
		samples.composedOf(system,entity3)
		samples.hasOne(entity1, entity2)
		samples.hasOneOrMore(entity2, entity3)
		samples.hasZeroOrMore(entity2, entity4)
		samples.hasZeroOrOne(entity1, entity5)
		20.times {
			def e = samples.entity(enterprise.key)
			samples.composedOf(system,e)
		}
		TownPlanPlantUMLItSystemDataEntitiesDiagramWriter writer = new TownPlanPlantUMLItSystemDataEntitiesDiagramWriter(targetDirectory, assetRepository, "timemachine", ContentOutputType.SVG)
		List<File> renderedFiles = []
		when:
		writer.write(townPlan, system, renderedFiles)
		then:
		renderedFiles.size() == 1
		renderedFiles.each { println it.absolutePath }
	}
}
