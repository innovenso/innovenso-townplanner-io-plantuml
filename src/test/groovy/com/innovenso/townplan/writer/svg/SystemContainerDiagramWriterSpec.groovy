package com.innovenso.townplan.writer.svg

import com.innovenso.townplan.api.value.aspects.ContentOutputType
import com.innovenso.townplan.api.value.aspects.FatherTimeType
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.domain.sample.SampleFactory
import com.innovenso.townplan.repository.AssetRepository
import com.innovenso.townplan.repository.FileSystemAssetRepository
import com.innovenso.townplan.writer.plantuml.TownPlanPlantUMLItSystemContainerDiagramWriter
import org.apache.commons.io.FileUtils
import spock.lang.Specification

import java.time.LocalDate


class SystemContainerDiagramWriterSpec extends Specification {
	private TownPlanImpl townPlan = new TownPlanImpl()
	private SampleFactory samples = new SampleFactory(townPlan)
	private static File targetDirectory = FileUtils.getTempDirectory()
	private static File assetDirectory = new File(targetDirectory, "assets")
	private static AssetRepository assetRepository = new FileSystemAssetRepository(assetDirectory.getAbsolutePath(), "http://localhost:8080/files/")

	def "time machine aware system container diagrams"() {
		given:
		def enterprise = samples.enterprise()
		def systemContext = samples.system("central", "Customer Website")
		def webapp = samples.container(systemContext, "webapp", "Web UI")
		def api = samples.container(systemContext, "api", "API")
		def database = samples.container(systemContext, "database", "Database")
		def stream = samples.container(systemContext, "stream", "Event Stream")
		def sso = samples.system("sso", "Single Sign On")
		def datalake = samples.system("datalake", "Data lake")
		def esb = samples.system("esb", "Enterprise Service Bus")
		def esbAdapter = samples.container(systemContext, "esbadapter", "ESB Adapter")
		def user = samples.actor(enterprise)
		samples.flow(user, webapp)
		samples.flow(user, systemContext)
		samples.flow(webapp, api)
		samples.flow(api, database)
		samples.flow(api, stream)
		samples.flow(api, sso)
		samples.flow(datalake, stream)
		samples.flow(datalake, systemContext)
		samples.flow(systemContext, sso)
		samples.flow(user, sso)
		samples.flow(api, esbAdapter)
		samples.flow(esbAdapter, esb)
		samples.flow(systemContext, esb)
		def one = LocalDate.of(2020,7,1)
		def two = LocalDate.of(2021,7,1)
		def three = LocalDate.of(2022,7,1)
		def a = LocalDate.of(2020, 8, 1)
		def b = LocalDate.of(2021, 8, 1)
		def pointOne = samples.keyPointInTime(one)
		def pointTwo = samples.keyPointInTime(two)
		def pointThree = samples.keyPointInTime(three)
		samples.fatherTime(datalake, a, FatherTimeType.ACTIVE)
		samples.fatherTime(stream, a, FatherTimeType.ACTIVE)
		samples.fatherTime(esb, b, FatherTimeType.DEATH)
		samples.fatherTime(esbAdapter, b, FatherTimeType.DEATH)
		samples.fatherTime(esb, a, FatherTimeType.RETIRED)
		samples.fatherTime(esbAdapter, a, FatherTimeType.RETIRED)
		samples.fatherTime(sso, a, FatherTimeType.DEATH)
		TownPlanPlantUMLItSystemContainerDiagramWriter writer = new TownPlanPlantUMLItSystemContainerDiagramWriter(targetDirectory, assetRepository, "timemachine", ContentOutputType.SVG)
		def modelOne = writer.getModel(systemContext, pointOne, townPlan)
		def modelTwo = writer.getModel(systemContext, pointTwo, townPlan)
		def modelThree = writer.getModel(systemContext, pointThree, townPlan)
		List<File> renderedFiles = []
		when:
		writer.write(townPlan, systemContext, renderedFiles)
		then:
		List.of(pointOne, pointTwo, pointThree).each {
			println esb.getLifecycle(it)
		}
		renderedFiles.size() == 4
		renderedFiles.each { println it.absolutePath }
	}
}
