package com.innovenso.townplan.writer.svg

import com.innovenso.townplan.api.value.aspects.ContentOutputType
import com.innovenso.townplan.api.value.it.DataEntity
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.domain.sample.SampleFactory
import com.innovenso.townplan.repository.AssetRepository
import com.innovenso.townplan.repository.FileSystemAssetRepository
import com.innovenso.townplan.writer.plantuml.TownPlanPlantUMLDataEntityWriter
import org.apache.commons.io.FileUtils
import spock.lang.Specification

class EntityRelationshipDiagramWriterSpec extends Specification {
	private TownPlanImpl townPlan = new TownPlanImpl()
	private SampleFactory samples = new SampleFactory(townPlan)
	private static File targetDirectory = FileUtils.getTempDirectory()
	private static File assetDirectory = new File(targetDirectory, "assets")
	private static AssetRepository assetRepository = new FileSystemAssetRepository(assetDirectory.getAbsolutePath(), "http://localhost:8080/files")

	def "entity relationship diagrams"() {
		given:
		def enterprise = samples.enterprise()
		DataEntity entity1 = samples.entity(enterprise.key)
		def entity2 = samples.entity(enterprise.key)
		def entity3 = samples.entity(enterprise.key)
		def entity4 = samples.entity(enterprise.key)
		def entity5 = samples.entity(enterprise.key)
		5.times {
			samples.entity(enterprise.key)
		}
		15.times {
			def e = samples.entity(enterprise.key)
			samples.hasZeroOrMore(entity1, e)
		}
		samples.hasOne(entity1, entity2)
		samples.hasOneOrMore(entity2, entity3)
		samples.hasZeroOrMore(entity2, entity4)
		samples.hasZeroOrOne(entity1, entity5)
		TownPlanPlantUMLDataEntityWriter writer = new TownPlanPlantUMLDataEntityWriter(targetDirectory, assetRepository, "timemachine", ContentOutputType.SVG)
		List<File> renderedFiles = []
		when:
		writer.write(townPlan, entity1, renderedFiles)
		then:
		renderedFiles.size() == 1
		renderedFiles.each { println it.absolutePath }
	}
}
