package com.innovenso.townplan.writer.svg

import com.innovenso.townplan.api.value.aspects.ContentOutputType
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.domain.sample.SampleFactory
import com.innovenso.townplan.repository.AssetRepository
import com.innovenso.townplan.repository.FileSystemAssetRepository
import com.innovenso.townplan.writer.plantuml.TownPlanPlantUMLArchitectureBuildingBlockWriter
import org.apache.commons.io.FileUtils
import spock.lang.Specification

class ArchitectureBuildingBlockDiagramWriterSpec extends Specification {
	private TownPlanImpl townPlan = new TownPlanImpl()
	private SampleFactory samples = new SampleFactory(townPlan)
	private static File targetDirectory = FileUtils.getTempDirectory()
	private static File assetDirectory = new File(targetDirectory, "assets")
	private static AssetRepository assetRepository = new FileSystemAssetRepository(assetDirectory.getAbsolutePath(), "http://localhost:8080/files")

	def "building block diagram"() {
		given:
		def platform = samples.platform("platform_1", "The Flexible Platform")
		4.times {
			samples.system(samples.id(), samples.title(), platform.key)
		}
		def enterprise = samples.enterprise()
		def otherSystem = samples.system()
		def capability0 = samples.capability(enterprise)
		def capability1 = samples.capability(enterprise, capability0)
		def buildingBlock = samples.buildingBlock(enterprise, capability1)
		samples.realizes(platform, buildingBlock)
		samples.realizes(otherSystem, buildingBlock)
		TownPlanPlantUMLArchitectureBuildingBlockWriter writer = new TownPlanPlantUMLArchitectureBuildingBlockWriter(targetDirectory, assetRepository, "buildingblock", ContentOutputType.SVG)
		List<File> renderedFiles = []
		when:
		writer.write(townPlan, buildingBlock, renderedFiles)
		then:
		renderedFiles.size() == 1
		renderedFiles.each { println it.absolutePath }
	}
}
