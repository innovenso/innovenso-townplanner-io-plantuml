package com.innovenso.townplan.writer.svg

import com.innovenso.townplan.api.value.aspects.ContentOutputType
import com.innovenso.townplan.api.value.aspects.FatherTimeType
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.domain.sample.SampleFactory
import com.innovenso.townplan.repository.AssetRepository
import com.innovenso.townplan.repository.FileSystemAssetRepository
import com.innovenso.townplan.writer.plantuml.TownPlanPlantUMLEnterpriseIntegrationMapWriter
import org.apache.commons.io.FileUtils
import spock.lang.Specification

import java.time.LocalDate

class EnterpriseIntegrationMapWriterSpec extends Specification {
	private TownPlanImpl townPlan = new TownPlanImpl()
	private SampleFactory samples = new SampleFactory(townPlan)
	private static File targetDirectory = FileUtils.getTempDirectory()
	private static File assetDirectory = new File(targetDirectory, "assets")
	private static AssetRepository assetRepository = new FileSystemAssetRepository(assetDirectory.getAbsolutePath(), "http://localhost:8080/files")

	def "time machine aware enterprise integration map"() {
		given:
		def enterprise = samples.enterprise("integration-map-test", "Integration Map Test")
		def capability = samples.capability(enterprise)
		def block = samples.buildingBlock(enterprise, capability)
		def staying1 = samples.system("system1", "System that is staying 1")
		def staying2 = samples.system("system2", "System that is staying 2")
		def dying1 = samples.system("system3", "System that is dying 1")
		def dying2 = samples.system("system4", "System that is dying 2")
		def new1 = samples.system("system5", "System that is being added 1")
		def new2 = samples.system("system6", "System that is being added 2")
		def dead = samples.system("system7", "System that is already dead")
		def stayingIntegration1 = samples.integration("integration1", "stays", staying1, staying2)
		def shouldDisappearIntegration2 = samples.integration("integration2", "should disappear when dying 1 is gone", staying2, dying1)
		def dyingIntegration1 = samples.integration("integration3", "should die after B", dying1, dying2)
		def appearing1 = samples.integration("integration4", "appears after A", new1, new2)
		def appearing2 = samples.integration("integration5", "appears after B", staying1, new2)
		samples.realizes(staying1, block)
		samples.realizes(staying2, block)
		samples.realizes(dying1, block)
		samples.realizes(dying2, block)
		samples.realizes(new1, block)
		samples.realizes(new2, block)
		samples.realizes(dead, block)

		def one = LocalDate.of(2020,7,1)
		def two = LocalDate.of(2021,7,1)
		def three = LocalDate.of(2022,7,1)
		def a = LocalDate.of(2020, 8, 1)
		def b = LocalDate.of(2021, 8, 1)
		def pointOne = samples.keyPointInTime(one)
		def pointTwo = samples.keyPointInTime(two)
		def pointThree = samples.keyPointInTime(three)
		samples.fatherTime(new1, a, FatherTimeType.ACTIVE)
		samples.fatherTime(new2, a, FatherTimeType.ACTIVE)
		samples.fatherTime(dying1, b, FatherTimeType.DEATH)
		samples.fatherTime(dying2, b, FatherTimeType.DEATH)
		samples.fatherTime(dying1, a, FatherTimeType.RETIRED)
		samples.fatherTime(dying2, a, FatherTimeType.RETIRED)
		samples.fatherTime(dead, a, FatherTimeType.DEATH)
		samples.fatherTime(dyingIntegration1, b, FatherTimeType.DEATH)
		samples.fatherTime(dyingIntegration1, a, FatherTimeType.RETIRED)
		samples.fatherTime(appearing1, a, FatherTimeType.ACTIVE)
		samples.fatherTime(appearing2, b, FatherTimeType.ACTIVE)
		TownPlanPlantUMLEnterpriseIntegrationMapWriter writer = new TownPlanPlantUMLEnterpriseIntegrationMapWriter(targetDirectory, assetRepository, "timemachine", ContentOutputType.SVG)
		def modelOne = writer.getModel(enterprise, pointOne, townPlan)
		def modelTwo = writer.getModel(enterprise, pointTwo, townPlan)
		def modelThree = writer.getModel(enterprise, pointThree, townPlan)
		List<File> renderedFiles = []
		when:
		writer.write(townPlan, enterprise, renderedFiles)
		println modelOne.systems
		println modelOne.integrations
		then:
		List.of(pointOne, pointTwo, pointThree).each {
			println dying1.getLifecycle(it)
		}
		renderedFiles.size() == 4
		renderedFiles.each { println it.absolutePath }
	}
}
