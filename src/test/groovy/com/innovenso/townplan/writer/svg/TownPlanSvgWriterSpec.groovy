package com.innovenso.townplan.writer.svg


import com.innovenso.townplan.api.value.aspects.ContentOutputType
import com.innovenso.townplan.api.value.aspects.LifecyleType
import com.innovenso.townplan.api.value.business.Enterprise
import com.innovenso.townplan.api.value.it.ItContainer
import com.innovenso.townplan.api.value.it.ItProject
import com.innovenso.townplan.api.value.it.ItProjectMilestone
import com.innovenso.townplan.api.value.it.ItSystem
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.domain.sample.SampleFactory
import com.innovenso.townplan.repository.AssetRepository
import com.innovenso.townplan.repository.FileSystemAssetRepository
import com.innovenso.townplan.writer.plantuml.TownPlanPlantUMLItProjectMilestoneAsIsSystemContainerDiagramWriter
import com.innovenso.townplan.writer.plantuml.TownPlanPlantUMLItProjectMilestoneToBeSystemContainerDiagramWriter
import org.apache.commons.io.FileUtils
import spock.lang.Specification

class TownPlanSvgWriterSpec extends Specification {
	private TownPlanImpl townPlan = new TownPlanImpl()
	private SampleFactory samples = new SampleFactory(townPlan)
	private static File targetDirectory = FileUtils.getTempDirectory()
	private static AssetRepository assetRepository = new FileSystemAssetRepository(FileUtils.getTempDirectory().getAbsolutePath(), "http://localhost:8080/files")


	def "as is and to be diagrams are written to an SVG file"() {
		given:
		Enterprise enterprise = samples.enterprise()
		ItSystem legacySystem = samples.system(LifecyleType.PHASEOUT)
		ItContainer legacyContainer1 = samples.container(legacySystem, LifecyleType.PHASEOUT)
		3.times { samples.addTechnology(legacyContainer1) }
		ItContainer legacyContainer2 = samples.container(legacySystem, LifecyleType.PHASEOUT)
		ItContainer legacyContainer3 = samples.container(legacySystem, LifecyleType.PHASEOUT)
		ItSystem newSystem = samples.system(LifecyleType.PLANNED)
		ItContainer newContainer1 = samples.container(newSystem, LifecyleType.PLANNED)
		ItContainer newContainer2 = samples.container(newSystem, LifecyleType.PLANNED)
		4.times { samples.addTechnology(newContainer2)}
		ItSystem remainingSystem1 = samples.system(LifecyleType.DECOMMISSIONED)
		ItSystem remainingSystem2 = samples.system(LifecyleType.DECOMMISSIONED)
		ItContainer remainingContainer1 = samples.container(remainingSystem2, LifecyleType.DECOMMISSIONED)
		2.times {samples.addTechnology(remainingContainer1)}
		samples.flow(legacySystem, remainingSystem1)
		samples.flow(legacyContainer1, legacyContainer2)
		samples.flow(legacyContainer1, legacyContainer3)
		samples.flow(legacyContainer1, remainingSystem1)
		samples.flow(legacyContainer1, remainingSystem2)
		samples.flow(legacyContainer1, remainingContainer1)
		samples.flow(newSystem, remainingSystem1)
		samples.flow(newSystem, remainingSystem2)
		samples.flow(newContainer1, newContainer2)
		samples.flow(newContainer1, remainingSystem1)
		samples.flow(newContainer1, remainingContainer1)
		ItProject project = samples.project(enterprise)
		ItProjectMilestone milestone = samples.milestone(project)
		samples.remove(milestone, legacySystem)
		samples.change(milestone, remainingSystem1)
		samples.change(milestone, remainingSystem2)
		samples.add(milestone, newSystem)
		TownPlanPlantUMLItProjectMilestoneAsIsSystemContainerDiagramWriter asisWriter = new TownPlanPlantUMLItProjectMilestoneAsIsSystemContainerDiagramWriter(targetDirectory, assetRepository, "project-milestone", ContentOutputType.PNG)
		TownPlanPlantUMLItProjectMilestoneToBeSystemContainerDiagramWriter tobeWriter = new TownPlanPlantUMLItProjectMilestoneToBeSystemContainerDiagramWriter(targetDirectory, assetRepository, "project-milestone", ContentOutputType.PNG)
		List<File> renderedFiles = []
		when:
		asisWriter.write(townPlan, milestone, renderedFiles)
		tobeWriter.write(townPlan, milestone, renderedFiles)
		then:
		renderedFiles.size() == 2
		renderedFiles.get(0).canRead()
		println renderedFiles.get(0).getAbsolutePath()
		renderedFiles.get(1).canRead()
		println renderedFiles.get(1).getAbsolutePath()
	}
}
