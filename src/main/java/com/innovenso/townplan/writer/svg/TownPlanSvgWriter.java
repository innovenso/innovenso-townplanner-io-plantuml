package com.innovenso.townplan.writer.svg;

import com.innovenso.townplan.api.value.aspects.ContentOutputType;
import com.innovenso.townplan.repository.AssetRepository;
import com.innovenso.townplan.writer.plantuml.AbstractTownPlanPlantUMLWriter;
import lombok.NonNull;

import java.io.File;

public class TownPlanSvgWriter extends AbstractTownPlanPlantUMLWriter {
	public TownPlanSvgWriter(@NonNull final String targetBasePath, @NonNull AssetRepository assetRepository) {
		super(new File(targetBasePath, "svg"), assetRepository, null, ContentOutputType.SVG);
	}
}
