package com.innovenso.townplan.writer.model;

import com.google.common.collect.Sets;
import com.innovenso.townplan.api.KeyPointInTime;
import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.business.BusinessActor;
import com.innovenso.townplan.api.value.it.ItContainer;
import com.innovenso.townplan.api.value.it.ItSystem;
import com.innovenso.townplan.api.value.it.ItSystemIntegration;
import com.innovenso.townplan.api.value.view.Step;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Log4j2
public class ITSystemIntegrationRelationshipDiagramModel extends ElementWriterModel<ItSystemIntegration> {
	public ITSystemIntegrationRelationshipDiagramModel(@NonNull ItSystemIntegration element,
			@NonNull KeyPointInTime keyPointInTime, @NonNull final TownPlan townPlan) {
		super(element, keyPointInTime, townPlan);
	}

	public List<Step> getSteps() {
		return getConcept().getSteps();
	}

	public Set<Element> getElements() {
		return getSteps().stream().map(Step::getRelationship)
				.flatMap(relationship -> Objects.equals(relationship.getSource(), relationship.getTarget())
						? Set.of(relationship.getSource()).stream()
						: Set.of(relationship.getSource(), relationship.getTarget()).stream())
				.collect(Collectors.toSet());
	}

	public Set<ItContainer> getContainers(final String system) {
		return getContainers().stream().filter(container -> Objects.equals(container.getSystem().getKey(), system))
				.collect(Collectors.toSet());
	}

	public Set<ItContainer> getContainers() {
		return getElements().stream().filter(ItContainer.class::isInstance).map(ItContainer.class::cast)
				.collect(Collectors.toSet());
	}

	public Set<ItSystem> getSystemContexts() {
		return getContainers().stream().map(ItContainer::getSystem).collect(Collectors.toSet());
	}

	public Set<ItSystem> getSystems() {
		return Sets.difference(getElements().stream().filter(ItSystem.class::isInstance).map(ItSystem.class::cast)
				.collect(Collectors.toSet()), getSystemContexts());
	}

	public Set<BusinessActor> getActors() {
		return getElements().stream().filter(BusinessActor.class::isInstance).map(BusinessActor.class::cast)
				.collect(Collectors.toSet());
	}
}
