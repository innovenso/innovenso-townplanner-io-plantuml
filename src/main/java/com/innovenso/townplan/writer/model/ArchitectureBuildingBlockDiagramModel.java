package com.innovenso.townplan.writer.model;

import com.google.common.collect.Sets;
import com.innovenso.townplan.api.KeyPointInTime;
import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.Relationship;
import com.innovenso.townplan.api.value.RelationshipType;
import com.innovenso.townplan.api.value.business.Enterprise;
import com.innovenso.townplan.api.value.it.ArchitectureBuildingBlock;
import com.innovenso.townplan.api.value.it.ItContainer;
import com.innovenso.townplan.api.value.it.ItPlatform;
import com.innovenso.townplan.api.value.it.ItSystem;
import com.innovenso.townplan.api.value.strategy.BusinessCapability;
import com.innovenso.townplan.domain.RelationshipImpl;
import lombok.NonNull;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

public class ArchitectureBuildingBlockDiagramModel extends ElementWriterModel<ArchitectureBuildingBlock> {
	private final boolean includeContainers;

	public ArchitectureBuildingBlockDiagramModel(@NonNull ArchitectureBuildingBlock element,
			@NonNull final KeyPointInTime keyPointInTime, @NonNull final TownPlan townPlan) {
		this(element, keyPointInTime, townPlan, true);
	}

	public ArchitectureBuildingBlockDiagramModel(@NonNull ArchitectureBuildingBlock element,
			@NonNull final KeyPointInTime keyPointInTime, @NonNull final TownPlan townPlan,
			final boolean includeContainers) {
		super(element, keyPointInTime, townPlan);
		this.includeContainers = includeContainers;
	}

	public Set<BusinessCapability> getBusinessCapabilities() {
		return getConcept().getDirectDependencies(BusinessCapability.class).stream().map(BusinessCapability.class::cast)
				.flatMap(cap -> getHierarchy(cap).stream()).collect(Collectors.toSet());
	}

	private Set<BusinessCapability> getHierarchy(BusinessCapability businessCapability) {
		return Sets.union(Set.of(businessCapability), getParents(businessCapability));
	}

	private Set<BusinessCapability> getParents(BusinessCapability businessCapability) {
		Optional<BusinessCapability> parent = businessCapability.getParent()
				.filter(BusinessCapability.class::isInstance).map(BusinessCapability.class::cast);
		if (parent.isPresent()) {
			BusinessCapability parentCapability = parent.get();
			return Sets.union(Set.of(parentCapability), getParents(parentCapability));
		} else
			return Set.of();
	}

	public Set<ItSystem> getSystems() {
		return Sets.union(getDirectSystems(), getPlatformSystems());
	}

	public Set<ItSystem> getDirectSystems() {
		return getConcept().getDirectDependencies(ItSystem.class).stream().map(ItSystem.class::cast)
				.collect(Collectors.toSet());
	}

	public Set<ItSystem> getOnlyDirectSystems() {
		return Sets.difference(getDirectSystems(), getPlatformSystems());
	}

	public Set<ItSystem> getPlatformSystems() {
		return getPlatforms().stream().flatMap(platform -> platform.getSystems().stream()).collect(Collectors.toSet());
	}

	public Set<ItPlatform> getPlatforms() {
		return getConcept().getDirectDependencies(ItPlatform.class).stream().map(ItPlatform.class::cast)
				.collect(Collectors.toSet());
	}

	public Set<ItContainer> getContainers() {
		if (includeContainers)
			return getSystems().stream().flatMap(itSystem -> itSystem.getContainers().stream())
					.collect(Collectors.toSet());
		else
			return Set.of();
	}

	public Set<Enterprise> getEnterprises() {
		return getBusinessCapabilities().stream()
				.filter(businessCapability -> businessCapability.getEnterprise() != null)
				.filter(businessCapability -> businessCapability.getParent().isEmpty())
				.map(BusinessCapability::getEnterprise).collect(Collectors.toSet());
	}

	public Set<Relationship> getContainerSystemRelationships() {
		return getContainers().stream()
				.map(container -> RelationshipImpl.builder().bidirectional(false)
						.relationshipType(RelationshipType.COMPOSITION).source(container).target(container.getSystem())
						.type("relationship").key(UUID.randomUUID().toString()).title("").build())
				.collect(Collectors.toSet());
	}

	public Set<Relationship> getCapabilityCapabilityRelationships() {
		return getBusinessCapabilities().stream()
				.filter(businessCapability -> businessCapability.getParent().isPresent())
				.map(businessCapability -> RelationshipImpl.builder().bidirectional(false)
						.relationshipType(RelationshipType.COMPOSITION).source(businessCapability)
						.target(businessCapability.getParent().get()).type("relationship")
						.key(UUID.randomUUID().toString()).title("").build())
				.collect(Collectors.toSet());
	}

	public Set<Relationship> getCapabilityEnterpriseRelationships() {
		return getBusinessCapabilities().stream()
				.filter(businessCapability -> businessCapability.getEnterprise() != null)
				.filter(businessCapability -> businessCapability.getParent().isEmpty())
				.map(businessCapability -> RelationshipImpl.builder().bidirectional(false)
						.relationshipType(RelationshipType.MEMBER).source(businessCapability)
						.target(businessCapability.getEnterprise()).key(UUID.randomUUID().toString()).title("")
						.type("relationship").build())
				.collect(Collectors.toSet());
	}

	public Set<Relationship> getSystemPlatformRelationships() {
		return getPlatformSystems().stream().filter(system -> system.getPlatform().isPresent())
				.map(system -> RelationshipImpl.builder().bidirectional(false)
						.relationshipType(RelationshipType.COMPOSITION).source(system)
						.target(system.getPlatform().get()).type("relationship").key(UUID.randomUUID().toString())
						.title("").build())
				.collect(Collectors.toSet());
	}

	public Set<Relationship> getPlatformBuildingBlockRelationships() {
		return getConcept().getRelationships(ItPlatform.class);
	}

	public Set<Relationship> getBusinessCapabilityRelationships() {
		return getConcept().getRelationships(BusinessCapability.class);
	}

	private boolean isDirectSystem(Relationship relationship) {
		return getDirectSystems().stream()
				.anyMatch(system -> system.getKey().equals(relationship.getSource().getKey()));
	}

	public Set<Relationship> getSystemBuildingBlockRelationships() {
		return getConcept().getRelationships(ItSystem.class).stream().filter(this::isDirectSystem)
				.collect(Collectors.toSet());
	}

	public Set<Relationship> getRelationships() {
		Set<Relationship> relationships = new HashSet<>();
		relationships.addAll(getPlatformBuildingBlockRelationships());
		relationships.addAll(getSystemBuildingBlockRelationships());
		relationships.addAll(getSystemPlatformRelationships());
		relationships.addAll(getBusinessCapabilityRelationships());
		relationships.addAll(getCapabilityCapabilityRelationships());
		relationships.addAll(getCapabilityEnterpriseRelationships());
		if (includeContainers)
			relationships.addAll(getContainerSystemRelationships());
		return relationships;
	}
}
