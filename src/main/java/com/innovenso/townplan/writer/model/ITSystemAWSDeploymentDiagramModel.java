package com.innovenso.townplan.writer.model;

import com.google.common.collect.Sets;
import com.innovenso.townplan.api.KeyPointInTime;
import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.Relationship;
import com.innovenso.townplan.api.value.deployment.aws.*;
import com.innovenso.townplan.api.value.enums.AwsInstanceType;
import com.innovenso.townplan.api.value.it.ItContainer;
import com.innovenso.townplan.api.value.it.ItSystem;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.tuple.Triple;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Log4j2
public class ITSystemAWSDeploymentDiagramModel extends ElementWriterModel<ItSystem> {
	private final Map<AwsInstanceType, String> instanceLogos;

	private final Set<AwsInstance> instances;
	private final Set<ItContainer> orphanContainers;
	private final Set<AwsAutoScalingGroup> autoScalingGroups;
	private final Set<AwsSubnet> subnets;
	private final Set<AwsVpc> vpcs;
	private final Set<AwsRegion> regions;
	private final Set<AwsRouteTable> routeTables;
	private final Set<AwsElasticIpAddress> ipAddresses;
	private final Set<Relationship> relationships;

	public ITSystemAWSDeploymentDiagramModel(@NonNull ItSystem element, @NonNull final KeyPointInTime keyPointInTime,
			@NonNull final TownPlan townPlan) {
		super(element, keyPointInTime, townPlan);
		this.instanceLogos = new HashMap<>();
		loadLogos();

		this.instances = new HashSet<>();
		loadContainerInstances();

		this.orphanContainers = new HashSet<>();
		loadOrphanContainers();

		addDependencies(AwsInstanceType.ECS_SERVICE, AwsInstanceType.ECS_CLUSTER);
		addDependencies(AwsInstanceType.ECS_SERVICE, AwsInstanceType.TARGET_GROUP);
		addDependencies(AwsInstanceType.TARGET_GROUP, AwsInstanceType.ELB);
		addDependencies(AwsInstanceType.TARGET_GROUP, AwsInstanceType.EC2);
		addDependencies(AwsInstanceType.EC2, AwsInstanceType.TARGET_GROUP);
		addDependencies(AwsInstanceType.LAMBDA, AwsInstanceType.LAMBDA);
		addDependencies(AwsInstanceType.BEANSTALK, AwsInstanceType.EC2);
		addDependencies(AwsInstanceType.BEANSTALK, AwsInstanceType.ELB);
		addDependencies(AwsInstanceType.BEANSTALK, AwsInstanceType.SQS);
		addDependencies(AwsInstanceType.ELASTICACHE_REPLICATION, AwsInstanceType.ELASTICACHE_CLUSTER);

		this.autoScalingGroups = new HashSet<>();
		loadAutoScalingGroups();
		addAutoscalingGroupInstances();

		this.subnets = new HashSet<>();
		loadSubnets();
		loadSubnetDependencies();

		this.routeTables = new HashSet<>();
		loadRouteTables();
		loadRouteTableDependencies();

		this.ipAddresses = new HashSet<>();
		loadIpAddresses();

		this.vpcs = new HashSet<>();
		loadVpcs();

		this.regions = new HashSet<>();
		loadRegions();

		this.relationships = new HashSet<>();
		loadRelationships();
	}

	private void loadAutoScalingGroups() {
		this.autoScalingGroups.addAll(Set.copyOf(instances).stream()
				.filter(instance -> instance.getInstanceType().equals(AwsInstanceType.ECS_CLUSTER))
				.peek(instance -> log.error("instance {}", instance)).flatMap(cluster -> cluster
						.getDirectDependencies(AwsAutoScalingGroup.class).stream().map(AwsAutoScalingGroup.class::cast))
				.collect(Collectors.toSet()));
	}

	private void loadContainerInstances() {
		this.instances.addAll(getConcept().getContainers().stream()
				.flatMap(container -> container.getDirectIncomingDependencies().stream())
				.filter(AwsInstance.class::isInstance).map(AwsInstance.class::cast).collect(Collectors.toSet()));
	}

	private void loadOrphanContainers() {
		this.orphanContainers.addAll(getConcept().getContainers().stream()
				.filter(itContainer -> itContainer.getDirectDependencies(AwsElement.class).isEmpty())
				.collect(Collectors.toSet()));
	}

	private void loadLogos() {
		instanceLogos.put(AwsInstanceType.RDS, "RDS");
		instanceLogos.put(AwsInstanceType.LAMBDA, "Lambda");
		instanceLogos.put(AwsInstanceType.EC2, "EC2");
		instanceLogos.put(AwsInstanceType.API_GATEWAY, "APIGateway2");
		instanceLogos.put(AwsInstanceType.DYNAMODB, "DynamoDB");
		instanceLogos.put(AwsInstanceType.EVENT_BRIDGE, "EventBridge");
		instanceLogos.put(AwsInstanceType.S3, "S3Bucket");
		instanceLogos.put(AwsInstanceType.SNS, "SNS");
		instanceLogos.put(AwsInstanceType.SQS, "SQS");
		instanceLogos.put(AwsInstanceType.ECS_SERVICE, "ECSService");
		instanceLogos.put(AwsInstanceType.DYNAMO_STREAM, "KinesisDataStreams");
		instanceLogos.put(AwsInstanceType.APP_SYNC, "AppSync");
		instanceLogos.put(AwsInstanceType.CLOUDWATCH, "CloudWatch");
		instanceLogos.put(AwsInstanceType.ELASTIC_SEARCH, "ElasticsearchService");
		instanceLogos.put(AwsInstanceType.KINESIS, "Kinesis");
		instanceLogos.put(AwsInstanceType.ECS_CLUSTER, "ElasticContainerService");
		instanceLogos.put(AwsInstanceType.ELB, "ElasticLoadBalancing");
		instanceLogos.put(AwsInstanceType.TARGET_GROUP, "AutoScalingGroup");
		instanceLogos.put(AwsInstanceType.BEANSTALK, "ElasticBeanstalk");
		instanceLogos.put(AwsInstanceType.ELASTICACHE_CLUSTER, "ElastiCacheNode");
		instanceLogos.put(AwsInstanceType.ELASTICACHE_REPLICATION, "ElastiCache");
		instanceLogos.put(AwsInstanceType.IGW, "VPCInternetGateway");
		instanceLogos.put(AwsInstanceType.TGW, "TransitGateway");
		instanceLogos.put(AwsInstanceType.NAT, "VPCNATGateway ");
		instanceLogos.put(AwsInstanceType.VPGW, "VPCVPNGateway");
		instanceLogos.put(AwsInstanceType.VPCE, "VPCEndpoints");
	}

	public ItSystem getCentralSystem() {
		return getConcept();
	}

	public Set<? extends Element> getSystems() {
		return Sets.union(getCentralSystem().getDirectDependencies(ItSystem.class),
				getCentralSystem().getChildDependencies(ItSystem.class));
	}

	public Set<ItContainer> getContainers() {
		return getCentralSystem().getContainers();
	}

	public Set<ItContainer> getOrphanContainers() {
		return orphanContainers;
	}

	public Set<AwsInstance> getAwsInstances() {
		return instances;
	}

	private void addDependencies(@NonNull final AwsInstanceType source, @NonNull final AwsInstanceType target) {
		log.info("Adding dependent instances of type {} connected to instances of type {}", target, source);
		Set.copyOf(this.instances).stream().filter(instance -> instance.getInstanceType().equals(source))
				.flatMap(service -> service.getDirectDependencies(AwsInstance.class).stream()
						.filter(AwsInstance.class::isInstance).map(AwsInstance.class::cast)
						.peek(i -> log.info("should add instance {}", i))
						.filter(i -> i.getInstanceType().equals(target)))
				.forEach(this.instances::add);
	}

	private void loadSubnets() {
		instances.stream().flatMap(instance -> instance.getSubnets().stream()).forEach(this.subnets::add);
	}

	private void loadSubnetDependencies() {
		Set<AwsInstanceType> allowedDependencies = Set.of(AwsInstanceType.NAT, AwsInstanceType.TGW,
				AwsInstanceType.VPCE);
		subnets.stream()
				.flatMap(subnet -> subnet.getDirectDependencies(AwsInstance.class).stream()
						.map(element -> (AwsInstance) element))
				.filter(instance -> allowedDependencies.contains(instance.getInstanceType())).forEach(instances::add);
	}

	private void loadRouteTableDependencies() {
		Set<AwsInstanceType> allowedDependencies = Set.of(AwsInstanceType.VPGW, AwsInstanceType.IGW,
				AwsInstanceType.VPCE);
		routeTables.stream()
				.flatMap(routeTable -> routeTable.getDirectDependencies(AwsInstance.class).stream()
						.map(element -> (AwsInstance) element))
				.filter(instance -> allowedDependencies.contains(instance.getInstanceType())).forEach(instances::add);
	}

	private void loadIpAddresses() {
		instances.stream().flatMap(instance -> instance.getDirectDependencies(AwsElasticIpAddress.class).stream()
				.map(element -> (AwsElasticIpAddress) element)).forEach(ipAddresses::add);
	}

	private void addAutoscalingGroupInstances() {
		this.instances.addAll(autoScalingGroups.stream().peek(asg -> log.error("autoscaling group {}", asg))
				.flatMap(asg -> asg.getInstances().stream()).peek(ec2 -> log.error("EC2 instance {}", ec2))
				.collect(Collectors.toSet()));
	}

	private void loadVpcs() {
		this.vpcs.addAll(getAwsInstances().stream().map(AwsInstance::getVpc).filter(Optional::isPresent)
				.map(Optional::get).collect(Collectors.toSet()));
	}

	private void loadRegions() {
		this.regions.addAll(Sets.union(vpcs.stream().map(AwsVpc::getRegion).collect(Collectors.toSet()),
				getAwsInstances().stream().map(AwsInstance::getRegion).filter(Optional::isPresent).map(Optional::get)
						.collect(Collectors.toSet())));
	}

	public Set<AwsAutoScalingGroup> getAutoscalingGroups(String vpcKey) {
		return autoScalingGroups.stream().filter(asg -> asg.getVpc().getKey().equals(vpcKey))
				.collect(Collectors.toSet());
	}

	private void loadRouteTables() {
		this.routeTables.addAll(subnets.stream().flatMap(subnet -> subnet.getDirectDependencies(AwsRouteTable.class)
				.stream().map(element -> (AwsRouteTable) element)).collect(Collectors.toSet()));
		this.routeTables
				.addAll(instances.stream().flatMap(instance -> instance.getDirectDependencies(AwsRouteTable.class)
						.stream().map(element -> (AwsRouteTable) element)).collect(Collectors.toSet()));
	}

	public Set<AwsRouteTable> getRouteTables(String vpcKey) {
		return routeTables.stream().filter(routeTable -> Objects.equals(routeTable.getVpc().getKey(), vpcKey))
				.collect(Collectors.toSet());
	}

	public Set<Relationship> getDependencies() {
		return relationships;
	}

	private void loadRelationships() {
		relationships.addAll(instances.stream()
				.flatMap(instance -> instance.getRelationships().stream().filter(this::includeRelationship))
				.collect(Collectors.toSet()));
		relationships.addAll(
				subnets.stream().flatMap(subnet -> subnet.getRelationships().stream().filter(this::includeRelationship))
						.collect(Collectors.toSet()));
		relationships.addAll(routeTables.stream()
				.flatMap(routeTable -> routeTable.getRelationships().stream().filter(this::includeRelationship))
				.collect(Collectors.toSet()));
		relationships.addAll(ipAddresses.stream()
				.flatMap(ipAddress -> ipAddress.getRelationships().stream().filter(this::includeRelationship))
				.collect(Collectors.toSet()));
	}

	private boolean includeRelationship(Relationship relationship) {
		return containsElement(relationship.getSource()) && containsElement(relationship.getTarget());
	}

	private boolean containsElement(Element element) {
		return instances.contains(element) || autoScalingGroups.contains(element) || subnets.contains(element)
				|| routeTables.contains(element) || ipAddresses.contains(element);
	}

	public Set<AwsRegion> getRegions() {
		return regions;
	}

	public Set<AwsVpc> getVpcs(String regionKey) {
		return vpcs.stream().filter(vpc -> vpc.getRegion().getKey().equals(regionKey)).collect(Collectors.toSet());
	}

	public Set<AwsSubnet> getSubnets(String vpcKey, String azKey) {
		return subnets.stream().filter(subnet -> subnet.getVpc().getKey().equals(vpcKey))
				.filter(subnet -> subnet.getAvailabilityZone().getKey().equals(azKey)).collect(Collectors.toSet());
	}

	public Set<AwsSecurityGroup> getNonEmptySecurityGroups(String subnet) {
		return instances.stream()
				.filter(awsInstance -> awsInstance.getSubnets().stream()
						.anyMatch(subnetKey -> subnetKey.getKey().equals(subnet)))
				.flatMap(instance -> instance.getSecurityGroups().stream()).collect(Collectors.toSet());
	}

	public Set<AwsInstance> getInstances(String subnet) {
		return instances.stream().filter(awsInstance -> awsInstance.getSubnets().stream()
				.anyMatch(subnetKey -> subnetKey.getKey().equals(subnet))).collect(Collectors.toSet());
	}

	public Set<AwsInstance> getRegionalInstances(String regionKey) {
		return instances.stream().filter(instance -> instance.getRegion().isPresent())
				.filter(instance -> instance.getVpc().isEmpty()).collect(Collectors.toSet());
	}

	public Set<AwsElasticIpAddress> getIpAddresses(String regionKey) {
		return ipAddresses.stream().filter(ipAddress -> Objects.equals(ipAddress.getRegion(), regionKey))
				.collect(Collectors.toSet());
	}

	public Set<AwsInstance> getVpcInstances(String vpc) {
		return instances.stream().filter(instance -> instance.getVpc().isPresent())
				.filter(instance -> instance.getSubnets().isEmpty()).collect(Collectors.toSet());
	}

	public Set<AwsInstance> getGlobalInstances() {
		return instances.stream().filter(instance -> instance.getVpc().isEmpty())
				.filter(instance -> instance.getRegion().isEmpty()).collect(Collectors.toSet());
	}

	public String getInstanceLogo(String awsInstanceTypeName) {
		AwsInstanceType instanceType = AwsInstanceType.getAwsInstanceType(awsInstanceTypeName);
		return instanceLogos.getOrDefault(instanceType, "EC2");
	}

	private Stream<AwsInstance> getContainerInstances(@NonNull final ItContainer container) {
		return container.getDirectIncomingDependencies().stream().filter(AwsInstance.class::isInstance)
				.map(AwsInstance.class::cast);
	}

	public Set<Triple<String, String, String>> getAutoScalingGroupMemberDependencies() {
		return autoScalingGroups.stream()
				.flatMap(asg -> asg.getInstances().stream().filter(instance -> instances.contains(instance))
						.map(instance -> Triple.of(asg.getKey(), "contains", instance.getKey())))
				.collect(Collectors.toSet());
	}

	public Set<Triple<String, String, String>> getRelationships() {
		return Sets.union(getAutoScalingGroupMemberDependencies(), getContainerInstanceDependencies());
	}

	public Set<Triple<String, String, String>> getContainerInstanceDependencies() {
		Set<Triple<String, String, String>> relationships = new HashSet<>();
		getContainers().stream().flatMap(container -> container.getRelationships(ItContainer.class).stream())
				.forEach(rel -> {
					Set<AwsInstance> sourceInstances = rel.getSource() instanceof ItContainer
							? getContainerInstances((ItContainer) rel.getSource()).filter(instances::contains)
									.collect(Collectors.toSet())
							: Set.of();
					Set<AwsInstance> targetInstances = rel.getSource() instanceof ItContainer
							? getContainerInstances((ItContainer) rel.getTarget()).filter(instances::contains)
									.collect(Collectors.toSet())
							: Set.of();
					sourceInstances.forEach(sourceInstance -> targetInstances.forEach(targetInstance -> relationships
							.add(Triple.of(sourceInstance.getKey(), rel.getTitle(), targetInstance.getKey()))));
				});
		return relationships;
	}
}
