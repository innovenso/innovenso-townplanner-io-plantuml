package com.innovenso.townplan.writer.model;

import com.google.common.collect.Sets;
import com.innovenso.townplan.api.KeyPointInTime;
import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.Relationship;
import com.innovenso.townplan.api.value.RelationshipType;
import com.innovenso.townplan.api.value.business.BusinessActor;
import com.innovenso.townplan.api.value.it.ItContainer;
import com.innovenso.townplan.api.value.it.ItProjectMilestone;
import com.innovenso.townplan.api.value.it.ItSystem;
import lombok.NonNull;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ProjectMilestoneSystemContainerDiagramModel extends ElementWriterModel<ItProjectMilestone> {
	private final boolean isToBe;

	public ProjectMilestoneSystemContainerDiagramModel(@NonNull ItProjectMilestone element,
			@NonNull KeyPointInTime keyPointInTime, @NonNull final TownPlan townPlan, boolean isToBe) {
		super(element, keyPointInTime, townPlan);
		this.isToBe = isToBe;
	}

	public boolean hasContainers(@NonNull final ItSystem system) {
		return getCentralSystems().stream().filter(cs -> cs.getKey().equals(system.getKey()))
				.anyMatch(ItSystem::hasContainers);
	}

	public Set<ItSystem> getCentralSystems() {
		return isToBe ? getToBeSystems() : getAsIsSystems();
	}

	private Set<ItSystem> getToBeSystems() {
		return Sets.difference(getConcept().getImpactedItSystems(), getConcept().getRemovedItSystems());
	}

	private Set<ItSystem> getAsIsSystems() {
		return Sets.difference(getConcept().getImpactedItSystems(), getConcept().getAddedItSystems());
	}

	public Set<? extends Element> getSystems() {
		Set<? extends Element> systems = Sets.union(
				getCentralSystems().stream().flatMap(system -> system.getDirectDependencies(ItSystem.class).stream())
						.collect(Collectors.toSet()),
				getCentralSystems().stream().flatMap(system -> system.getChildDependencies(ItSystem.class).stream())
						.collect(Collectors.toSet()));
		return systems.stream()
				.filter(system -> getCentralSystems().stream().noneMatch(cs -> system.getKey().equals(system.getKey())))
				.collect(Collectors.toSet());
	}

	public Set<? extends Element> getActors() {
		return Sets.union(
				getCentralSystems().stream()
						.flatMap(system -> system.getDirectDependencies(BusinessActor.class).stream())
						.collect(Collectors.toSet()),
				getCentralSystems().stream()
						.flatMap(system -> system.getChildDependencies(BusinessActor.class).stream())
						.collect(Collectors.toSet()));
	}

	public Set<Relationship> getSystemRelationships() {
		return Sets
				.union(getCentralSystems().stream()
						.flatMap(system -> system.getRelationships(ItSystem.class).stream().filter(
								this::areBothElementsInTheDiagram))
						.collect(Collectors.toSet()),
						getCentralSystems().stream().flatMap(system -> system.getRelationships(BusinessActor.class)
								.stream().filter(this::areBothElementsInTheDiagram)).collect(Collectors.toSet()));
	}

	public Set<Relationship> getContainerRelationships() {
		return getCentralSystems().stream().flatMap(system -> system.getContainers().stream())
				.flatMap(container -> container.getRelationships().stream().filter(this::areBothElementsInTheDiagram)
						.filter(relationship -> shouldIncludeContainerRelationship(container, relationship)))
				.collect(Collectors.toSet());
	}

	public Set<Relationship> getRelationships() {
		return Sets.union(getSystemRelationships(), getContainerRelationships());
	}

	public Set<Relationship> getFlows() {
		return getRelationships().stream().filter(this::isFlow).filter(this::includeFlow).collect(Collectors.toSet());
	}

	private boolean isFlow(Relationship relationship) {
		return relationship.getRelationshipType().equals(RelationshipType.FLOW);
	}

	private boolean includeFlow(Relationship relationship) {
		return !isSystemContext(relationship.getSource()) && !isSystemContext(relationship.getTarget());
	}

	private boolean isSystemContext(Element element) {
		return getCentralSystems().stream().filter(system -> system.getKey().equals(element.getKey()))
				.anyMatch(ItSystem::hasContainers);
	}

	private boolean areBothElementsInTheDiagram(Relationship relationship) {
		return isElementInTheDiagram(relationship.getSource()) && isElementInTheDiagram(relationship.getTarget());
	}

	private boolean isElementInTheDiagram(Element element) {
		return getCentralSystems().stream().anyMatch(system -> system.getKey().equals(element.getKey()))
				|| getCentralSystems().stream().flatMap(system -> system.getContainers().stream())
						.anyMatch(container -> container.getKey().equals(element.getKey()))
				|| getActors().stream().anyMatch(actor -> actor.getKey().equals(element.getKey()));
	}

	private boolean shouldIncludeContainerRelationship(@NonNull final ItContainer container,
			@NonNull final Relationship relationship) {
		return isInternalContainerRelationship(container, relationship)
				|| isExternalSystemOrActorRelationship(container, relationship);
	}

	private boolean isInternalContainerRelationship(@NonNull final ItContainer container,
			@NonNull final Relationship relationship) {
		if (relationship.isCircular())
			return true;
		Element other = relationship.getOther(container);
		return other.getParent().map(
				element -> getCentralSystems().stream().anyMatch(system -> system.getKey().equals(element.getKey())))
				.orElse(false);
	}

	private boolean isExternalSystemOrActorRelationship(@NonNull final ItContainer container,
			@NonNull final Relationship relationship) {
		return Stream.of(ItSystem.class, BusinessActor.class)
				.anyMatch(elementClass -> elementClass.isInstance(relationship.getOther(container)));
	}
}
