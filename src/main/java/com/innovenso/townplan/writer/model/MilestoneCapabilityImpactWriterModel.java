package com.innovenso.townplan.writer.model;

import com.innovenso.townplan.api.KeyPointInTime;
import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.it.ItProject;
import com.innovenso.townplan.api.value.it.ItProjectMilestone;
import com.innovenso.townplan.api.value.strategy.BusinessCapability;
import lombok.NonNull;

import java.util.*;

public class MilestoneCapabilityImpactWriterModel extends ElementWriterModel<ItProjectMilestone> {
	public MilestoneCapabilityImpactWriterModel(@NonNull ItProjectMilestone element,
			@NonNull final KeyPointInTime keyPointInTime, @NonNull final TownPlan townPlan) {
		super(element, keyPointInTime, townPlan);
	}

	public ItProject getProject() {
		return getConcept().getProject();
	}

	public List<BusinessCapability> getAddedTree() {
		return getTree(getConcept().getAddedBusinessCapabilities());
	}

	public List<BusinessCapability> getRemovedTree() {
		return getTree(getConcept().getRemovedBusinessCapabilities());
	}

	public List<BusinessCapability> getChangedTree() {
		return getTree(getConcept().getChangedBusinessCapabilities());
	}

	private List<BusinessCapability> getTree(final @NonNull Set<BusinessCapability> impacted) {
		final Set<BusinessCapability> input = new HashSet<>();
		impacted.forEach(capability -> {
			input.add(capability);
			addParent(capability, input);
		});

		List<BusinessCapability> tree = new ArrayList<>();

		input.stream().filter(cap -> cap.getLevel() == 0)
				.forEach(level0 -> addCapabilityAndChildren(level0, input, tree));

		return tree;
	}

	private void addCapabilityAndChildren(BusinessCapability capability, Set<BusinessCapability> source,
			List<BusinessCapability> target) {
		target.add(capability);
		capability.getChildCapabilities().stream().filter(source::contains)
				.sorted(Comparator.comparing(BusinessCapability::getSortKey))
				.forEach(child -> addCapabilityAndChildren(child, source, target));
	}

	private void addParent(@NonNull final BusinessCapability capability,
			@NonNull final Set<BusinessCapability> target) {
		capability.getParentCapability().ifPresent(parent -> {
			target.add(parent);
			addParent(parent, target);
		});
	}
}
