package com.innovenso.townplan.writer.model;

import com.innovenso.townplan.api.KeyPointInTime;
import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.business.Enterprise;
import com.innovenso.townplan.api.value.strategy.BusinessCapability;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

@Log4j2
public class EnterpriseMindMapModel extends ElementWriterModel<Enterprise> {
	public EnterpriseMindMapModel(@NonNull Enterprise element, @NonNull final KeyPointInTime keyPointInTime,
			@NonNull final TownPlan townPlan) {
		super(element, keyPointInTime, townPlan);
	}

	public List<BusinessCapability> getBusinessCapabilities() {
		final List<BusinessCapability> capabilities = new ArrayList<>();
		getCapabilities(capabilities, getConcept().getChildren());
		return capabilities;
	}

	private void getCapabilities(@NonNull List<BusinessCapability> result, @NonNull final Set<Element> children) {
		children.stream().filter(BusinessCapability.class::isInstance).map(BusinessCapability.class::cast)
				.sorted(Comparator.comparing(BusinessCapability::getSortKey)).forEach(cap -> {
					result.add(cap);
					getCapabilities(result, cap.getChildren());
				});
	}
}
