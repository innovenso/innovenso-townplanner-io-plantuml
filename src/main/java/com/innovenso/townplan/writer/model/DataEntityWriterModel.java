package com.innovenso.townplan.writer.model;

import com.google.common.collect.Sets;
import com.innovenso.townplan.api.KeyPointInTime;
import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.Relationship;
import com.innovenso.townplan.api.value.RelationshipType;
import com.innovenso.townplan.api.value.enums.EntityType;
import com.innovenso.townplan.api.value.it.DataEntity;
import lombok.Getter;
import lombok.NonNull;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class DataEntityWriterModel extends ElementWriterModel<DataEntity> {
	@Getter
	private final Set<DataEntity> relatedEntities = new HashSet<>();

	public DataEntityWriterModel(@NonNull DataEntity element, @NonNull KeyPointInTime keyPointInTime,
			@NonNull TownPlan townPlan) {
		super(element, keyPointInTime, townPlan);
		collectRelatedEntities(element, relatedEntities);
	}

	public DataEntity getEntity() {
		return getConcept();
	}

	public Set<DataEntity> getEntities() {
		return Sets.union(Set.of(getEntity()), getRelatedEntities());
	}

	private void collectRelatedEntities(@NonNull final DataEntity entity,
			@NonNull final Set<DataEntity> relatedEntities) {
		entity.getDirectDependencies(DataEntity.class).stream().filter(DataEntity.class::isInstance)
				.map(DataEntity.class::cast).filter(e -> !relatedEntities.contains(e)).forEach(e -> {
					relatedEntities.add(e);
					collectRelatedEntities(e, relatedEntities);
				});
	}

	private Set<Relationship> getRelationships(@NonNull final RelationshipType relationshipType) {
		return getRelatedEntities().stream().flatMap(entity -> entity.getRelationships(DataEntity.class).stream())
				.filter(relationship -> relationship.getRelationshipType().equals(relationshipType))
				.collect(Collectors.toSet());
	}

	public Set<Relationship> getHasOnes() {
		return getRelationships(RelationshipType.HAS_ONE);
	}

	public Set<Relationship> getHasOneOrMores() {
		return getRelationships(RelationshipType.HAS_ONE_OR_MORE);
	}

	public Set<Relationship> getHasZeroOrMores() {
		return getRelationships(RelationshipType.HAS_ZERO_OR_MORE);
	}

	public Set<Relationship> getHasZeroOrOnes() {
		return getRelationships(RelationshipType.HAS_ZERO_OR_ONE);
	}

	public Set<EntityType> getEntityTypes() {
		return Arrays.stream(EntityType.values()).collect(Collectors.toSet());
	}

	public String getColor(EntityType entityType) {
		switch (entityType) {
			case VALUE_OBJECT :
			case ENTITY :
				return "#fcf6b0";
			case EVENT :
				return "#fd9d4a";
			case QUERY :
			case COMMAND :
				return "#26c0e7";
			case READ_MODEL :
				return "#d5f693";
			default :
				return "#ffffff";
		}
	}
}
