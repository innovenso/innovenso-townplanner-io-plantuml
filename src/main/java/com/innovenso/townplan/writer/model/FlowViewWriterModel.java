package com.innovenso.townplan.writer.model;

import com.google.common.collect.Sets;
import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.business.BusinessActor;
import com.innovenso.townplan.api.value.it.ItContainer;
import com.innovenso.townplan.api.value.it.ItSystem;
import com.innovenso.townplan.api.value.view.FlowView;
import com.innovenso.townplan.api.value.view.Step;
import lombok.NonNull;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class FlowViewWriterModel extends ViewWriterModel<FlowView> {
	public FlowViewWriterModel(@NonNull final FlowView view) {
		super(view);
	}

	public Set<Element> getElements() {
		return getView().getElements();
	}

	public List<Step> getSteps() {
		return getView().getSteps();
	}

	public Set<ItContainer> getContainers(final String system) {
		return getView().getElements(ItContainer.class).stream()
				.filter(container -> Objects.equals(container.getSystem().getKey(), system))
				.collect(Collectors.toSet());
	}

	public Set<ItContainer> getContainers() {
		return getView().getElements(ItContainer.class);
	}

	public Set<ItSystem> getSystems() {
		return Sets.difference(getView().getElements(ItSystem.class), getSystemContexts());
	}

	public Set<ItSystem> getSystemContexts() {
		return getView().getElements(ItContainer.class).stream().map(ItContainer::getSystem)
				.collect(Collectors.toSet());
	}

	public Set<BusinessActor> getActors() {
		return getView().getElements(BusinessActor.class);
	}
}
