package com.innovenso.townplan.writer.model;

import com.innovenso.townplan.api.KeyPointInTime;
import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.strategy.BusinessCapability;
import lombok.NonNull;

import java.util.*;

public class BusinessCapabilityWriterModel extends ElementWriterModel<BusinessCapability> {

	public BusinessCapabilityWriterModel(@NonNull BusinessCapability element,
			@NonNull final KeyPointInTime keyPointInTime, @NonNull TownPlan townPlan) {
		super(element, keyPointInTime, townPlan);
	}

	public List<BusinessCapability> getTree() {
		final List<BusinessCapability> tree = new ArrayList<>();
		final List<BusinessCapability> parents = new ArrayList<>();
		addParent(getConcept(), parents);
		Collections.reverse(parents);
		tree.addAll(parents);
		tree.add(getConcept());
		addDescendants(tree, getConcept().getChildren());
		return tree;
	}

	private void addParent(@NonNull final BusinessCapability capability,
			@NonNull final List<BusinessCapability> target) {
		capability.getParentCapability().ifPresent(parent -> {
			target.add(parent);
			addParent(parent, target);
		});
	}

	private void addDescendants(@NonNull List<BusinessCapability> target, @NonNull final Set<Element> children) {
		children.stream().filter(BusinessCapability.class::isInstance).map(BusinessCapability.class::cast)
				.sorted(Comparator.comparing(BusinessCapability::getSortKey)).forEach(cap -> {
					target.add(cap);
					addDescendants(target, cap.getChildren());
				});
	}
}
