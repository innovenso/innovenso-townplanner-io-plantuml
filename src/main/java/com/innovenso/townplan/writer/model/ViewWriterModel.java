package com.innovenso.townplan.writer.model;

import com.innovenso.townplan.api.value.view.View;
import lombok.NonNull;

public class ViewWriterModel<T extends View> {
	private final T view;

	public ViewWriterModel(@NonNull final T view) {
		this.view = view;
	}

	public T getView() {
		return view;
	}

	public String getKey() {
		return view.getKey();
	}

	public String getTitle() {
		return view.getTitle();
	}

	public String getDescription() {
		return view.getDescription() == null ? "" : view.getDescription();
	}

	public String getCategory() {
		return view.getCategory();
	}
}
