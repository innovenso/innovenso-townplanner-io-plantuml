package com.innovenso.townplan.writer.model;

import com.innovenso.townplan.api.KeyPointInTime;
import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.Relationship;
import com.innovenso.townplan.api.value.RelationshipType;
import com.innovenso.townplan.api.value.it.ItProjectMilestone;
import com.innovenso.townplan.api.value.it.ItSystem;
import com.innovenso.townplan.api.value.it.ItSystemIntegration;
import com.innovenso.townplan.domain.RelationshipImpl;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Log4j2
public class ItProjectMilestoneIntegrationOverviewModel extends ElementWriterModel<ItProjectMilestone> {

	public ItProjectMilestoneIntegrationOverviewModel(@NonNull ItProjectMilestone element,
			@NonNull final KeyPointInTime keyPointInTime, @NonNull final TownPlan townPlan) {
		super(element, keyPointInTime, townPlan);
	}

	public ItProjectMilestone getMilestone() {
		return getConcept();
	}

	public Set<ItSystemIntegration> getIntegrations() {
		return getMilestone().getRelationships(ItSystemIntegration.class).stream().filter(this::isPresentInMilestone)
				.map(relationship -> relationship.getOther(getMilestone()))
				.filter(ItSystemIntegration.class::isInstance).map(ItSystemIntegration.class::cast)
				.collect(Collectors.toSet());
	}

	public Set<ItSystem> getSystems() {
		return getIntegrations().stream().flatMap(integration -> integration.getSystems().stream())
				.collect(Collectors.toSet());
	}

	private boolean isPresentInMilestone(@NonNull final Relationship relationship) {
		return Set.of(RelationshipType.IMPACT_CHANGE, RelationshipType.IMPACT_CREATE,
				RelationshipType.IMPACT_REMAINS_UNCHANGED).contains(relationship.getRelationshipType());
	}

	public Set<Relationship> getRelationships() {
		return getIntegrations().stream()
				.map(integration -> RelationshipImpl.builder().key(UUID.randomUUID().toString())
						.source(integration.getSourceSystem()).target(integration.getTargetSystem())
						.title(getRelationshipTitle(integration)).bidirectional(true)
						.relationshipType(RelationshipType.FLOW).type("relationship").build())
				.peek(relationship -> log.info(relationship.getTextRepresentation())).collect(Collectors.toSet());
	}

	private String getRelationshipTitle(ItSystemIntegration integration) {
		return integration.getTitle() + "[" + integration.getImplementingSystems().stream().map(ItSystem::getTitle)
				.collect(Collectors.joining(", ")) + "]";
	}
}
