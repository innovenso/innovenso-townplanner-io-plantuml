package com.innovenso.townplan.writer.model;

import com.innovenso.townplan.api.KeyPointInTime;
import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.Relationship;
import com.innovenso.townplan.api.value.RelationshipType;
import com.innovenso.townplan.api.value.business.BusinessActor;
import com.innovenso.townplan.api.value.business.Enterprise;
import com.innovenso.townplan.api.value.it.*;
import com.innovenso.townplan.api.value.strategy.BusinessCapability;
import com.innovenso.townplan.domain.RelationshipImpl;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Log4j2
public class EnterpriseTownPlanModel extends ElementWriterModel<Enterprise> {
	public EnterpriseTownPlanModel(@NonNull Enterprise element, @NonNull final KeyPointInTime keyPointInTime,
			@NonNull final TownPlan townPlan) {
		super(element, keyPointInTime, townPlan);
	}

	public List<BusinessCapability> getBusinessCapabilities() {
		return getAllBusinessCapabilities().filter(cap -> cap.getLevel() <= 2).collect(Collectors.toList());
	}

	public List<BusinessCapability> getBusinessFunctions() {
		return getAllBusinessCapabilities().filter(cap -> cap.getLevel() > 2).collect(Collectors.toList());
	}

	private Stream<BusinessCapability> getAllBusinessCapabilities() {
		Set<BusinessCapability> level0 = getChildren().stream().filter(this::isRendered)
				.filter(BusinessCapability.class::isInstance).map(BusinessCapability.class::cast)
				.collect(Collectors.toSet());
		return Stream.concat(level0.stream(), level0.stream().flatMap(BusinessCapability::getDescendantStream))
				.sorted(Comparator.comparing(BusinessCapability::getSortKey));
	}

	public List<ItProject> getProjects() {
		return getConcept().getChildren().stream().filter(this::isRendered).filter(ItProject.class::isInstance)
				.map(ItProject.class::cast).collect(Collectors.toList());
	}

	public List<ItProjectMilestone> getMilestones() {
		return getProjects().stream().filter(this::isRendered).flatMap(project -> project.getMilestones().stream())
				.collect(Collectors.toList());
	}

	public List<BusinessActor> getBusinessPeople() {
		return actors().filter(BusinessActor::isPerson).collect(Collectors.toList());
	}

	public List<BusinessActor> getSquads() {
		return actors().filter(BusinessActor::isSquad).collect(Collectors.toList());
	}

	public List<BusinessActor> getTribes() {
		return actors().filter(BusinessActor::isTribe).collect(Collectors.toList());
	}

	public List<BusinessActor> getAlliances() {
		return actors().filter(BusinessActor::isAlliance).collect(Collectors.toList());
	}

	public List<BusinessActor> getAllActors() {
		return actors().collect(Collectors.toList());
	}

	private Stream<BusinessActor> actors() {
		return getConcept().getChildren().stream().filter(this::isRendered).filter(BusinessActor.class::isInstance)
				.map(BusinessActor.class::cast).sorted(Comparator.comparing(BusinessActor::getTitle));
	}

	public Set<ArchitectureBuildingBlock> getBuildingBlocks() {
		return getCapabilityBuildingBlockRelationships().stream().map(Relationship::getSource)
				.map(ArchitectureBuildingBlock.class::cast).filter(this::isRendered).collect(Collectors.toSet());
	}

	public Set<ItSystem> getSystems() {
		return getBuildingBlockSystemRelationships().stream().map(Relationship::getSource).map(ItSystem.class::cast)
				.filter(this::isRendered).collect(Collectors.toSet());
	}

	public Set<ItContainer> getContainers() {
		return getSystems().stream().flatMap(system -> system.getContainers().stream()).filter(this::isRendered)
				.collect(Collectors.toSet());
	}

	public Set<Relationship> getFlows() {
		final Set<Relationship> flows = new HashSet<>();
		flows.addAll(getSystems().stream().flatMap(system -> system.getRelationships().stream())
				.filter(relationship -> relationship.getRelationshipType().equals(RelationshipType.FLOW))
				.collect(Collectors.toSet()));
		flows.addAll(getContainers().stream().flatMap(container -> container.getRelationships().stream())
				.filter(relationship -> relationship.getRelationshipType().equals(RelationshipType.FLOW))
				.collect(Collectors.toSet()));
		flows.addAll(getAllActors().stream().flatMap(actor -> actor.getRelationships().stream())
				.filter(relationship -> relationship.getRelationshipType().equals(RelationshipType.FLOW))
				.collect(Collectors.toSet()));
		return flows.stream().filter(this::isRelationshipRendered).collect(Collectors.toSet());
	}

	public Set<Relationship> getContainerSystemRelationships() {
		return getContainers().stream()
				.map(container -> RelationshipImpl.builder().bidirectional(false)
						.relationshipType(RelationshipType.COMPOSITION).source(container).target(container.getSystem())
						.type("relationship").key(UUID.randomUUID().toString()).title("").build())
				.filter(this::isRelationshipRendered).collect(Collectors.toSet());
	}

	public Set<Relationship> getCapabilityBuildingBlockRelationships() {
		return getBusinessCapabilities().stream()
				.flatMap(businessCapability -> businessCapability
						.getIncomingRelationships(ArchitectureBuildingBlock.class).stream()
						.filter(this::isRelationshipRendered))
				.collect(Collectors.toSet());
	}

	public Set<Relationship> getBuildingBlockSystemRelationships() {
		return getBuildingBlocks()
				.stream().flatMap(architectureBuildingBlock -> architectureBuildingBlock
						.getIncomingRelationships(ItSystem.class).stream().filter(this::isRelationshipRendered))
				.collect(Collectors.toSet());
	}

	public Set<ArchitectureBuildingBlock> getBuildingBlocks(final @NonNull String capabilityKey) {
		return getCapabilityAndChildren(capabilityKey).stream()
				.flatMap(cap -> cap.getIncomingRelationships(ArchitectureBuildingBlock.class).stream())
				.filter(relationship -> relationship.getRelationshipType().equals(RelationshipType.REALIZATION))
				.map(Relationship::getSource).filter(this::isRendered)
				.filter(ArchitectureBuildingBlock.class::isInstance).map(ArchitectureBuildingBlock.class::cast)
				.collect(Collectors.toSet());
	}

	private List<BusinessCapability> getCapabilityAndChildren(final @NonNull String capabilityKey) {
		return getBusinessCapabilities().stream().filter(element -> capabilityKey.equals(element.getKey())).findFirst() // any
																														// business
																														// capability
																														// up
																														// to
																														// level
																														// 2
																														// that
																														// has
																														// the
																														// given
																														// key
				.map(capability -> {
					List<BusinessCapability> hierarchy = new ArrayList<>();
					hierarchy.add(capability);
					getCapabilities(hierarchy, capability.getChildren());
					return hierarchy;
				}).orElse(List.of());
	}

	public List<BusinessCapability> getBusinessCapabilitiesWithDataEntities() {
		return getBusinessCapabilities().stream()
				.filter(cap -> cap.getChildren().stream().anyMatch(DataEntity.class::isInstance))
				.sorted(Comparator.comparing(Element::getSortKey)).collect(Collectors.toList());
	}

	public List<DataEntity> getDataEntities(@NonNull final String capabilityKey) {
		return dataEntityStream()
				.filter(entity -> entity.getBusinessCapability() != null
						&& entity.getBusinessCapability().getKey().equals(capabilityKey))
				.sorted(Comparator.comparing(DataEntity::getSortKey)).collect(Collectors.toList());
	}

	public boolean isDataEntityImpacted(@NonNull final String entityKey, @NonNull final String projectKey) {
		Optional<ItProject> project = getProjects().stream().filter(p -> p.getKey().equals(projectKey)).findFirst();
		if (project.isEmpty())
			return false;

		return dataEntityStream().filter(entity -> entity.getKey().equals(entityKey))
				.anyMatch(entity -> project.get().getImpactedDataEntities().contains(entity));
	}

	public Stream<DataEntity> dataEntityStream() {
		return getConcept().getChildren().stream().filter(this::isRendered).filter(DataEntity.class::isInstance)
				.map(DataEntity.class::cast);
	}

	public List<DataEntity> getDataEntities() {
		return dataEntityStream().collect(Collectors.toList());
	}

	public Set<Relationship> getCapabilityCapabilityRelationships() {
		return getBusinessCapabilities().stream()
				.filter(businessCapability -> businessCapability.getParent().isPresent())
				.map(businessCapability -> RelationshipImpl.builder().bidirectional(false)
						.relationshipType(RelationshipType.COMPOSITION).source(businessCapability)
						.target(businessCapability.getParent().get()).type("relationship")
						.key(UUID.randomUUID().toString()).title("").build())
				.filter(this::isRelationshipRendered).collect(Collectors.toSet());
	}

	public Set<Relationship> getCapabilityEnterpriseRelationships() {
		return getBusinessCapabilities().stream().filter(businessCapability -> businessCapability.getParent().isEmpty())
				.map(businessCapability -> RelationshipImpl.builder().bidirectional(false)
						.relationshipType(RelationshipType.MEMBER).source(businessCapability).target(getConcept())
						.key(UUID.randomUUID().toString()).title("").type("relationship").build())
				.filter(this::isRelationshipRendered).collect(Collectors.toSet());
	}

	public Set<Relationship> getRelationships() {
		Set<Relationship> relationships = new HashSet<>();
		relationships.addAll(getCapabilityEnterpriseRelationships());
		relationships.addAll(getCapabilityCapabilityRelationships());
		relationships.addAll(getCapabilityBuildingBlockRelationships());
		relationships.addAll(getBuildingBlockSystemRelationships());
		relationships.addAll(getContainerSystemRelationships());
		return relationships;
	}

	private void getCapabilities(@NonNull List<BusinessCapability> result, @NonNull final Set<Element> children) {
		children.stream().filter(this::isRendered).filter(BusinessCapability.class::isInstance)
				.map(BusinessCapability.class::cast).sorted(Comparator.comparing(BusinessCapability::getTitle))
				.forEach(cap -> {
					result.add(cap);
					if (cap.getLevel() < 2) {
						getCapabilities(result, cap.getChildren());
					}
				});
	}

	public List<BusinessCapability> getLevelZeroBusinessCapabilities() {
		return getConcept().getChildren().stream().filter(this::isRendered).filter(BusinessCapability.class::isInstance)
				.map(BusinessCapability.class::cast).sorted(Comparator.comparing(BusinessCapability::getSortKey))
				.collect(Collectors.toList());
	}

	public List<BusinessCapability> getLevelOneBusinessCapabilities() {
		return getAllBusinessCapabilities().filter(cap -> cap.getLevel() == 1).collect(Collectors.toList());
	}

	public List<BusinessCapability> getLevelTwoBusinessCapabilities() {
		return getAllBusinessCapabilities().filter(cap -> cap.getLevel() == 2).collect(Collectors.toList());
	}

	public List<BusinessCapability> getLevelOneBusinessCapabilities(String parentKey) {
		return getLevelZeroBusinessCapabilities().stream().filter(cap -> cap.getKey().equals(parentKey)).findFirst()
				.map(businessCapability -> businessCapability.getChildren().stream()
						.filter(BusinessCapability.class::isInstance).map(BusinessCapability.class::cast)
						.sorted(Comparator.comparing(BusinessCapability::getSortKey)).collect(Collectors.toList()))
				.orElseGet(List::of);
	}

	public List<SystemContainerDiagramModel> getSystemContainerDiagramModels() {
		return getSystems().stream()
				.map(system -> new SystemContainerDiagramModel(system, getPointInTime(), getTownPlan()))
				.collect(Collectors.toList());
	}

	public Set<ItSystemIntegration> getIntegrations() {
		return getSystems().stream().flatMap(itSystem -> itSystem.getIntegrations().stream()).filter(this::isRendered)
				.collect(Collectors.toSet());
	}

	public Set<Relationship> getSystemIntegrationRelationships() {
		return getIntegrations().stream()
				.map(integration -> RelationshipImpl.builder().key(UUID.randomUUID().toString())
						.source(integration.getSourceSystem()).target(integration.getTargetSystem())
						.title(getRelationshipTitle(integration)).description(integration.getDescription())
						.bidirectional(true).relationshipType(RelationshipType.FLOW).type("relationship").build())
				.peek(relationship -> log.info(relationship.getTextRepresentation()))
				.filter(this::isRelationshipRendered).collect(Collectors.toSet());
	}

	private String getRelationshipTitle(ItSystemIntegration integration) {
		return integration.getTitle() + "[" + integration.getImplementingSystems().stream().map(ItSystem::getTitle)
				.collect(Collectors.joining(", ")) + "]";
	}
}
