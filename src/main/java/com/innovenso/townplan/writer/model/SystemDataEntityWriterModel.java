package com.innovenso.townplan.writer.model;

import com.google.common.collect.Sets;
import com.innovenso.townplan.api.KeyPointInTime;
import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.Relationship;
import com.innovenso.townplan.api.value.RelationshipType;
import com.innovenso.townplan.api.value.enums.EntityType;
import com.innovenso.townplan.api.value.it.DataEntity;
import com.innovenso.townplan.api.value.it.ItSystem;
import com.innovenso.townplan.domain.RelationshipImpl;
import lombok.Getter;
import lombok.NonNull;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

public class SystemDataEntityWriterModel extends ElementWriterModel<ItSystem> {
	@Getter
	private final Set<DataEntity> relatedEntities = new HashSet<>();

	public SystemDataEntityWriterModel(@NonNull ItSystem element, @NonNull KeyPointInTime keyPointInTime,
			@NonNull TownPlan townPlan) {
		super(element, keyPointInTime, townPlan);
		collectRelatedEntities(element, relatedEntities);
	}

	public ItSystem getSystem() {
		return getConcept();
	}

	public Set<DataEntity> getReadModels() {
		return getEntities(EntityType.READ_MODEL);
	}

	public Set<DataEntity> getEntities() {
		return getEntities(EntityType.ENTITY);
	}

	public Set<DataEntity> getValueObjects() {
		return getEntities(EntityType.VALUE_OBJECT);
	}

	public Set<DataEntity> getDomainModel() {
		return Sets.union(getEntities(), getValueObjects());
	}

	public Set<DataEntity> getEvents() {
		return getEntities(EntityType.EVENT);
	}

	public Set<DataEntity> getCommands() {
		return getEntities(EntityType.COMMAND);
	}

	public Set<DataEntity> getQueries() {
		return getEntities(EntityType.QUERY);
	}

	private Set<DataEntity> getEntities(@NonNull final EntityType entityType) {
		return relatedEntities.stream().filter(dataEntity -> dataEntity.getEntityType().equals(entityType))
				.collect(Collectors.toSet());
	}

	private void collectRelatedEntities(@NonNull final Element element,
			@NonNull final Set<DataEntity> relatedEntities) {
		element.getDirectDependencies(DataEntity.class).stream().filter(DataEntity.class::isInstance)
				.map(DataEntity.class::cast).filter(e -> !relatedEntities.contains(e)).forEach(e -> {
					relatedEntities.add(e);
					collectRelatedEntities(e, relatedEntities);
				});
		element.getChildren().stream().filter(DataEntity.class::isInstance).map(DataEntity.class::cast).forEach(e -> {
			relatedEntities.add(e);
			collectRelatedEntities(e, relatedEntities);
		});
	}

	private Set<Relationship> getRelationships(@NonNull final RelationshipType relationshipType) {
		return getRelatedEntities().stream().flatMap(entity -> entity.getRelationships(DataEntity.class).stream())
				.filter(relationship -> relationship.getRelationshipType().equals(relationshipType))
				.collect(Collectors.toSet());
	}

	public Set<Relationship> getSystemRelationships() {
		return Sets.union(getCompositionRelationships(), getChildRelationships());
	}

	private Set<Relationship> getCompositionRelationships() {
		return getSystem().getRelationships(DataEntity.class).stream()
				.filter(relationship -> relationship.getRelationshipType().equals(RelationshipType.COMPOSITION))
				.collect(Collectors.toSet());
	}

	private Set<Relationship> getChildRelationships() {
		return relatedEntities.stream()
				.filter(dataEntity -> dataEntity.getParent().map(parent -> parent.getKey().equals(getSystem().getKey()))
						.orElse(false))
				.map(dataEntity -> RelationshipImpl.builder().key(UUID.randomUUID().toString().replaceAll("-", ""))
						.title("is master").relationshipType(RelationshipType.COMPOSITION).source(getSystem())
						.target(dataEntity).build())
				.collect(Collectors.toSet());
	}

	public Set<Relationship> getHasOnes() {
		return getRelationships(RelationshipType.HAS_ONE);
	}

	public Set<Relationship> getHasOneOrMores() {
		return getRelationships(RelationshipType.HAS_ONE_OR_MORE);
	}

	public Set<Relationship> getHasZeroOrMores() {
		return getRelationships(RelationshipType.HAS_ZERO_OR_MORE);
	}

	public Set<Relationship> getHasZeroOrOnes() {
		return getRelationships(RelationshipType.HAS_ZERO_OR_ONE);
	}

	public Set<EntityType> getEntityTypes() {
		return Arrays.stream(EntityType.values()).collect(Collectors.toSet());
	}

	public String getColor(EntityType entityType) {
		switch (entityType) {
			case VALUE_OBJECT :
			case ENTITY :
				return "#fcf6b0";
			case EVENT :
				return "#fd9d4a";
			case QUERY :
			case COMMAND :
				return "#26c0e7";
			case READ_MODEL :
				return "#d5f693";
			default :
				return "#ffffff";
		}
	}
}
