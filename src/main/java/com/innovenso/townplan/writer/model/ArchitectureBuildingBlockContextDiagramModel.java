package com.innovenso.townplan.writer.model;

import com.google.common.collect.Sets;
import com.innovenso.townplan.api.KeyPointInTime;
import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.Relationship;
import com.innovenso.townplan.api.value.RelationshipType;
import com.innovenso.townplan.api.value.business.BusinessActor;
import com.innovenso.townplan.api.value.it.ArchitectureBuildingBlock;
import lombok.NonNull;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ArchitectureBuildingBlockContextDiagramModel extends ElementWriterModel<ArchitectureBuildingBlock> {
	public ArchitectureBuildingBlockContextDiagramModel(@NonNull ArchitectureBuildingBlock element,
			@NonNull final KeyPointInTime keyPointInTime, @NonNull final TownPlan townPlan) {
		super(element, keyPointInTime, townPlan);
	}

	public ArchitectureBuildingBlock getCentralBuildingBlock() {
		return getConcept();
	}

	public Set<ArchitectureBuildingBlock> getBuildingBlocks() {
		return buildingBlockRelationships().stream()
				.map(relationship -> relationship.getOther(getCentralBuildingBlock()))
				.filter(ArchitectureBuildingBlock.class::isInstance).map(ArchitectureBuildingBlock.class::cast)
				.collect(Collectors.toSet());
	}

	public Set<BusinessActor> getActors() {
		return Sets.union(getBuildingBlocks(), Set.of(getCentralBuildingBlock())).stream().flatMap(this::actorStream)
				.collect(Collectors.toSet());
	}

	public Set<Relationship> getRelationships() {
		return Sets.union(buildingBlockRelationships(), actorRelationships());
	}

	private Stream<BusinessActor> actorStream(ArchitectureBuildingBlock architectureBuildingBlock) {
		return architectureBuildingBlock.getRelationships(BusinessActor.class).stream()
				.filter(relationship -> relationship.getRelationshipType().equals(RelationshipType.FLOW))
				.map(relationship -> relationship.getOther(architectureBuildingBlock))
				.filter(BusinessActor.class::isInstance).map(BusinessActor.class::cast);
	}

	private Set<Relationship> buildingBlockRelationships() {
		return getCentralBuildingBlock().getRelationships(ArchitectureBuildingBlock.class).stream()
				.filter(relationship -> relationship.getRelationshipType().equals(RelationshipType.FLOW))
				.collect(Collectors.toSet());
	}

	private Set<Relationship> actorRelationships() {
		return Sets.union(getBuildingBlocks(), Set.of(getCentralBuildingBlock())).stream()
				.flatMap(architectureBuildingBlock -> architectureBuildingBlock.getRelationships(BusinessActor.class)
						.stream()
						.filter(relationship -> relationship.getRelationshipType().equals(RelationshipType.FLOW)))
				.collect(Collectors.toSet());
	}
}
