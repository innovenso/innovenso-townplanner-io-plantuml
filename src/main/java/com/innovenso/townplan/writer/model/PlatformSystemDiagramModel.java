package com.innovenso.townplan.writer.model;

import com.google.common.collect.Sets;
import com.innovenso.townplan.api.KeyPointInTime;
import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.Relationship;
import com.innovenso.townplan.api.value.RelationshipType;
import com.innovenso.townplan.api.value.business.BusinessActor;
import com.innovenso.townplan.api.value.it.ItPlatform;
import com.innovenso.townplan.api.value.it.ItSystem;
import lombok.NonNull;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PlatformSystemDiagramModel extends ElementWriterModel<ItPlatform> {
	public PlatformSystemDiagramModel(@NonNull ItPlatform element, @NonNull final KeyPointInTime keyPointInTime,
			@NonNull final TownPlan townPlan) {
		super(element, keyPointInTime, townPlan);
	}

	public boolean hasSystems() {
		return !getInnerSystems().isEmpty();
	}

	public ItPlatform getPlatform() {
		return getConcept();
	}

	private Stream<ItSystem> innerSystems() {
		return getPlatform().getSystems().stream().filter(this::isRendered);
	}

	private Stream<ItSystem> outerSystems() {
		return innerSystems()
				.flatMap(
						system -> system.getDirectDependencies(ItSystem.class).stream().map(ItSystem.class::cast)
								.filter(s -> s.getPlatform()
										.map(itPlatform -> itPlatform.getKey() != getPlatform().getKey()).orElse(true)))
				.filter(this::isRendered);
	}

	public Set<ItSystem> getInnerSystems() {
		return innerSystems().collect(Collectors.toSet());
	}

	public Set<ItSystem> getOuterSystems() {
		return outerSystems().collect(Collectors.toSet());
	}

	public Set<ItSystem> getAllSystems() {
		return Sets.union(getInnerSystems(), getOuterSystems());
	}

	private Stream<Relationship> flows(Class<? extends Element> otherClass) {
		return getAllSystems().stream()
				.flatMap(system -> system.getRelationships(otherClass).stream()
						.filter(relationship -> relationship.getRelationshipType().equals(RelationshipType.FLOW)))
				.filter(this::isRendered);
	}

	private Stream<Relationship> systemFlows() {
		return flows(ItSystem.class);
	}

	private Stream<Relationship> actorFlows() {
		return flows(BusinessActor.class);
	}

	public Set<BusinessActor> getActors() {
		return actorFlows()
				.map(relationship -> relationship.getSource() instanceof BusinessActor
						? relationship.getSource()
						: relationship.getTarget())
				.filter(BusinessActor.class::isInstance).map(BusinessActor.class::cast).collect(Collectors.toSet());
	}

	public Set<Relationship> getFlows() {
		return Sets.union(systemFlows().collect(Collectors.toSet()), actorFlows().collect(Collectors.toSet()));
	}

	boolean isRendered(Relationship relationship, Element source) {
		return isRendered(relationship.getOther(source));
	}

	boolean isRendered(Relationship relationship) {
		return isRendered(relationship.getSource()) && isRendered(relationship.getTarget());
	}
}
