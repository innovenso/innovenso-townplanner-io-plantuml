package com.innovenso.townplan.writer.model;

import com.google.common.collect.Sets;
import com.innovenso.townplan.api.KeyPointInTime;
import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.ModelComponent;
import com.innovenso.townplan.api.value.Relationship;
import com.innovenso.townplan.api.value.RelationshipType;
import com.innovenso.townplan.api.value.business.BusinessActor;
import com.innovenso.townplan.api.value.it.ItSystem;
import com.innovenso.townplan.api.value.strategy.BusinessCapability;
import lombok.NonNull;

import java.util.Comparator;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class SystemContextDiagramModel extends ElementWriterModel<ItSystem> {
	public SystemContextDiagramModel(@NonNull ItSystem element, @NonNull KeyPointInTime keyPointInTime,
			@NonNull final TownPlan townPlan) {
		super(element, keyPointInTime, townPlan);
	}

	public ItSystem getCentralSystem() {
		return getConcept();
	}

	public Set<ItSystem> getDependentSystems() {
		return getCentralSystem().getDirectDependencies(ItSystem.class).stream().filter(ItSystem.class::isInstance)
				.map(ItSystem.class::cast).collect(Collectors.toSet());
	}

	public Set<ItSystem> getSystems() {
		return Sets.union(Set.of(getCentralSystem()), getDependentSystems());
	}

	public Set<? extends Element> getActors() {
		return getCentralSystem().getRelationships(BusinessActor.class).stream()
				.filter(relationship -> relationship.getRelationshipType().equals(RelationshipType.FLOW))
				.map(relationship -> relationship.getOther(getCentralSystem())).collect(Collectors.toSet());
	}

	public Set<BusinessCapability> getBusinessCapabilities() {
		return getSystems().stream().map(this::getBusinessCapability).filter(Optional::isPresent).map(Optional::get)
				.collect(Collectors.toSet());
	}

	private Optional<BusinessCapability> getBusinessCapability(ItSystem itSystem) {
		return itSystem.getBuildingBlocks().stream()
				.flatMap(architectureBuildingBlock -> architectureBuildingBlock.getRealizedCapabilities().stream())
				.max(Comparator.comparing(BusinessCapability::getLevel));
	}

	public Set<ItSystem> getSystems(String businessCapabilityKey) {
		return getSystems().stream().filter(system -> getBusinessCapability(system).map(ModelComponent::getKey)
				.orElse("").equals(businessCapabilityKey)).collect(Collectors.toSet());
	}

	public Set<ItSystem> getSystemsWithoutCapabilities() {
		return getSystems().stream().filter(system -> getBusinessCapability(system).isEmpty())
				.collect(Collectors.toSet());
	}

	public Set<Relationship> getRelationships() {
		return Sets.union(getCentralSystem().getRelationships(ItSystem.class),
				getCentralSystem().getRelationships(BusinessActor.class));
	}

	public Set<Relationship> getFlows() {
		return getRelationships().stream()
				.filter(relationship -> relationship.getRelationshipType().equals(RelationshipType.FLOW))
				.collect(Collectors.toSet());
	}
}
