package com.innovenso.townplan.writer.png;

import com.innovenso.townplan.api.value.aspects.ContentOutputType;
import com.innovenso.townplan.repository.AssetRepository;
import com.innovenso.townplan.writer.plantuml.AbstractTownPlanPlantUMLWriter;
import lombok.NonNull;

import java.io.File;

public class TownPlanPngWriter extends AbstractTownPlanPlantUMLWriter {
	public TownPlanPngWriter(@NonNull final String targetBasePath, @NonNull AssetRepository assetRepository) {
		super(new File(targetBasePath, "png"), assetRepository, null, ContentOutputType.PNG);
	}
}
