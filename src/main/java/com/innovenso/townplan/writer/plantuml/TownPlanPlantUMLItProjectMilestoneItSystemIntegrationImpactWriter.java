package com.innovenso.townplan.writer.plantuml;

import com.innovenso.townplan.api.value.aspects.ContentOutputType;
import com.innovenso.townplan.api.value.it.ItProjectMilestone;
import com.innovenso.townplan.repository.AssetRepository;
import lombok.NonNull;

import java.io.File;

public class TownPlanPlantUMLItProjectMilestoneItSystemIntegrationImpactWriter
		extends
			AbstractTownPlanPlantUMLConceptWriter<ItProjectMilestone> {

	public TownPlanPlantUMLItProjectMilestoneItSystemIntegrationImpactWriter(@NonNull File targetBaseDirectory,
			@NonNull AssetRepository assetRepository, String folderName, @NonNull ContentOutputType contentOutputType) {
		super(targetBaseDirectory, assetRepository, folderName == null ? "project-milestone" : folderName,
				contentOutputType);
	}

	@Override
	protected String getPlantUMLTemplateName(@NonNull ItProjectMilestone element) {
		return "milestone-integrations";
	}

	@Override
	protected String getOutputFileName(@NonNull ItProjectMilestone element) {
		return super.getOutputFileName(element) + "-system-integration-impact";
	}

	@Override
	protected String getContentDistributionTitle(ItProjectMilestone concept, ContentOutputType contentOutputType) {
		return "System Integration Impact Diagram (" + contentOutputType.getLabel() + ")";
	}
}
