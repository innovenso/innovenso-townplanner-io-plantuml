package com.innovenso.townplan.writer.plantuml;

import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.command.AddContentDistributionAspectCommand;
import com.innovenso.townplan.api.value.aspects.ContentOutputType;
import com.innovenso.townplan.api.value.view.View;
import com.innovenso.townplan.domain.TownPlanImpl;
import com.innovenso.townplan.repository.AssetRepository;
import com.innovenso.townplan.writer.model.ViewWriterModel;
import com.innovenso.townplan.writer.pipeline.freemarker.FreemarkerRenderer;
import com.innovenso.townplan.writer.pipeline.plantuml.PlantUMLRenderer;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import net.sourceforge.plantuml.FileFormat;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Log4j2
public abstract class AbstractTownPlanPlantUMLViewWriter<T extends View> implements TownPlanPlantUMLViewWriter<T> {
	private final FreemarkerRenderer freemarkerRenderer;
	private final PlantUMLRenderer plantUMLRenderer;
	private final AssetRepository assetRepository;
	private final String folderName;
	private final ContentOutputType contentOutputType;
	private final Map<ContentOutputType, FileFormat> fileFormats;
	private final File targetBaseDirectory;

	public AbstractTownPlanPlantUMLViewWriter(@NonNull final File targetBaseDirectory,
			@NonNull final AssetRepository assetRepository, @NonNull final String folderName,
			@NonNull final ContentOutputType contentOutputType) {
		this.freemarkerRenderer = new FreemarkerRenderer("plantuml");
		this.contentOutputType = contentOutputType;
		this.folderName = folderName;
		this.targetBaseDirectory = new File(targetBaseDirectory, folderName);
		if (!this.targetBaseDirectory.exists())
			this.targetBaseDirectory.mkdirs();

		this.fileFormats = Map.of(ContentOutputType.PNG, FileFormat.PNG, ContentOutputType.SVG, FileFormat.SVG,
				ContentOutputType.LATEX, FileFormat.EPS, ContentOutputType.VISIO, FileFormat.VDX);
		this.plantUMLRenderer = new PlantUMLRenderer(fileFormats.get(contentOutputType));
		this.assetRepository = assetRepository;
	}

	@Override
	public void write(TownPlan townPlan, T view, List<File> renderedFiles) {
		Map<String, Object> model = Map.of("view", getModel(view));
		Optional<File> plantUmlSpecification = freemarkerRenderer.write(model, getPlantUMLTemplateName(view) + ".ftl");
		plantUmlSpecification.ifPresent(file -> {
			Optional<File> renderedOutputFile = plantUMLRenderer.write(file);
			renderedOutputFile.ifPresent(render -> {
				final File resultingFile = new File(targetBaseDirectory,
						getOutputFileName(view) + fileFormats.get(contentOutputType).getFileSuffix());
				try {
					FileUtils.copyFile(render, resultingFile);
					renderedFiles.add(resultingFile);
					saveAssetAndRecordCdnUrl(resultingFile, view, townPlan);
				} catch (IOException e) {
					log.error(e);
				}
			});
		});
	}

	protected abstract String getContentDistributionTitle(T view, ContentOutputType contentOutputType);

	private void saveAssetAndRecordCdnUrl(@NonNull File render, @NonNull final T view,
			@NonNull final TownPlan townPlan) {
		assetRepository.write(render, getOutputPath(view));
		assetRepository.getCdnUrl(getOutputPath(view)).ifPresent(cdnUrl -> {
			if (townPlan instanceof TownPlanImpl tpi) {
				tpi.execute(AddContentDistributionAspectCommand.builder().url(cdnUrl).modelComponentKey(view.getKey())
						.type(contentOutputType).title(getContentDistributionTitle(view, contentOutputType)).build());
			}
		});
	}

	protected String getPlantUMLTemplateName(@NonNull final T view) {
		return view.getCategory();
	}

	protected String getOutputFileName(@NonNull final T view) {
		return view.getKey();
	}

	public String getOutputPath(@NonNull final T view) {
		return folderName + "/" + getOutputFileName(view) + fileFormats.get(contentOutputType).getFileSuffix();
	}

	protected ViewWriterModel<T> getModel(T view) {
		return new ViewWriterModel<>(view);
	}
}
