package com.innovenso.townplan.writer.plantuml;

import com.innovenso.townplan.api.KeyPointInTime;
import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.aspects.ContentOutputType;
import com.innovenso.townplan.api.value.it.ItProjectMilestone;
import com.innovenso.townplan.repository.AssetRepository;
import com.innovenso.townplan.writer.model.ConceptWriterModel;
import com.innovenso.townplan.writer.model.ItProjectMilestoneIntegrationOverviewModel;
import lombok.NonNull;

import java.io.File;

public class TownPlanPlantUMLItProjectMilestoneIntegrationMapWriter
		extends
			AbstractTownPlanPlantUMLConceptWriter<ItProjectMilestone> {

	public TownPlanPlantUMLItProjectMilestoneIntegrationMapWriter(@NonNull File targetBaseDirectory,
			@NonNull AssetRepository assetRepository, String folderName, @NonNull ContentOutputType contentOutputType) {
		super(targetBaseDirectory, assetRepository, folderName == null ? "project-milestone" : folderName,
				contentOutputType);
	}

	@Override
	protected String getPlantUMLTemplateName(@NonNull ItProjectMilestone element) {
		return "milestone-integration-map";
	}

	@Override
	protected String getOutputFileName(@NonNull ItProjectMilestone element) {
		return super.getOutputFileName(element) + "-system-integration-map";
	}

	@Override
	protected String getContentDistributionTitle(ItProjectMilestone concept, ContentOutputType contentOutputType) {
		return "Integration Map (" + contentOutputType.getLabel() + ")";
	}

	@Override
	protected ConceptWriterModel<ItProjectMilestone> getModel(ItProjectMilestone element, KeyPointInTime keyPointInTime,
			@NonNull final TownPlan townPlan) {
		return new ItProjectMilestoneIntegrationOverviewModel(element, keyPointInTime, townPlan);
	}
}
