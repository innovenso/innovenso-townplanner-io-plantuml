package com.innovenso.townplan.writer.plantuml;

import com.innovenso.townplan.api.value.aspects.ContentOutputType;
import com.innovenso.townplan.api.value.it.ItSystem;
import com.innovenso.townplan.repository.AssetRepository;
import lombok.NonNull;

import java.io.File;

public class TownPlanPlantUMLItSystemToSystemIntegrationDiagramWriter
		extends
			AbstractTownPlanPlantUMLConceptWriter<ItSystem> {

	public TownPlanPlantUMLItSystemToSystemIntegrationDiagramWriter(@NonNull File targetBaseDirectory,
			@NonNull AssetRepository assetRepository, String folderName, @NonNull ContentOutputType contentOutputType) {
		super(targetBaseDirectory, assetRepository, folderName == null ? "system" : folderName, contentOutputType);
	}

	@Override
	protected String getContentDistributionTitle(ItSystem concept, ContentOutputType contentOutputType) {
		return "System Integration Diagram (" + contentOutputType.getLabel() + ")";
	}

	@Override
	protected String getPlantUMLTemplateName(@NonNull ItSystem element) {
		return "system-integration-diagram";
	}

	@Override
	protected String getOutputFileName(@NonNull ItSystem element) {
		return super.getOutputFileName(element) + "-integrations";
	}
}
