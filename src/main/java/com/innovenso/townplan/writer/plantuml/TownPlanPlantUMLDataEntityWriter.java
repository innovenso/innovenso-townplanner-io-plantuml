package com.innovenso.townplan.writer.plantuml;

import com.innovenso.townplan.api.KeyPointInTime;
import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.aspects.ContentOutputType;
import com.innovenso.townplan.api.value.it.DataEntity;
import com.innovenso.townplan.repository.AssetRepository;
import com.innovenso.townplan.writer.model.ConceptWriterModel;
import com.innovenso.townplan.writer.model.DataEntityWriterModel;
import lombok.NonNull;

import java.io.File;

public class TownPlanPlantUMLDataEntityWriter extends AbstractTownPlanPlantUMLConceptWriter<DataEntity> {
	public TownPlanPlantUMLDataEntityWriter(@NonNull File targetBaseDirectory, @NonNull AssetRepository assetRepository,
			String folderName, @NonNull ContentOutputType contentOutputType) {
		super(targetBaseDirectory, assetRepository, folderName, contentOutputType);
	}

	@Override
	protected String getContentDistributionTitle(DataEntity concept, ContentOutputType contentOutputType) {
		return "Domain Diagram";
	}

	@Override
	protected String getPlantUMLTemplateName(@NonNull DataEntity element) {
		return "entity";
	}

	@Override
	protected ConceptWriterModel<DataEntity> getModel(DataEntity element, KeyPointInTime pointInTime,
			@NonNull TownPlan townPlan) {
		return new DataEntityWriterModel(element, pointInTime, townPlan);
	}
}
