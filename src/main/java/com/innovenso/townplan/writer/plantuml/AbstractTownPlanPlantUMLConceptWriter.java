package com.innovenso.townplan.writer.plantuml;

import com.innovenso.townplan.api.KeyPointInTime;
import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.command.AddContentDistributionAspectCommand;
import com.innovenso.townplan.api.value.Concept;
import com.innovenso.townplan.api.value.aspects.ContentOutputType;
import com.innovenso.townplan.domain.KeyPointInTimeImpl;
import com.innovenso.townplan.domain.TownPlanImpl;
import com.innovenso.townplan.repository.AssetRepository;
import com.innovenso.townplan.writer.model.ConceptWriterModel;
import com.innovenso.townplan.writer.pipeline.freemarker.FreemarkerRenderer;
import com.innovenso.townplan.writer.pipeline.plantuml.PlantUMLRenderer;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import net.sourceforge.plantuml.FileFormat;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Log4j2
public abstract class AbstractTownPlanPlantUMLConceptWriter<T extends Concept>
		implements
			TownPlanPlantUMLConceptWriter<T> {
	private final FreemarkerRenderer freemarkerRenderer;
	private final PlantUMLRenderer plantUMLRenderer;
	private final AssetRepository assetRepository;
	private final String folderName;
	private final ContentOutputType contentOutputType;
	private final Map<ContentOutputType, FileFormat> fileFormats;
	private final File targetBaseDirectory;

	public AbstractTownPlanPlantUMLConceptWriter(@NonNull final File targetBaseDirectory,
			@NonNull final AssetRepository assetRepository, final String folderName,
			@NonNull final ContentOutputType contentOutputType) {
		this.freemarkerRenderer = new FreemarkerRenderer("plantuml");
		this.contentOutputType = contentOutputType;
		this.folderName = folderName == null ? "element" : folderName;
		this.targetBaseDirectory = new File(targetBaseDirectory, this.folderName);
		if (!this.targetBaseDirectory.exists())
			this.targetBaseDirectory.mkdirs();

		this.fileFormats = Map.of(ContentOutputType.PNG, FileFormat.PNG, ContentOutputType.SVG, FileFormat.SVG,
				ContentOutputType.LATEX, FileFormat.EPS, ContentOutputType.VISIO, FileFormat.VDX);
		this.plantUMLRenderer = new PlantUMLRenderer(fileFormats.get(contentOutputType));
		this.assetRepository = assetRepository;
	}

	@Override
	public void write(@NonNull final TownPlan townPlan, @NonNull final T element, @NonNull List<File> renderedFiles) {
		if (supportsTimeTravel()) {
			log.info("writer supports time travel, let's write for all points in time");
			townPlan.getKeyPointsInTime()
					.forEach(keyPointInTime -> write(townPlan, element, renderedFiles, keyPointInTime));
		} else {
			write(townPlan, element, renderedFiles, KeyPointInTimeImpl.builder().date(LocalDate.now())
					.title("There's no time like the present").build());
		}
	}

	protected void write(@NonNull final TownPlan townPlan, @NonNull final T element, @NonNull List<File> renderedFiles,
			@NonNull KeyPointInTime keyPointInTime) {
		log.info("writing plantuml diagram for element {} point in time {}", element.getKey(),
				keyPointInTime.getDate());
		Map<String, Object> model = Map.of("element", getModel(element, keyPointInTime, townPlan));
		Optional<File> plantUmlSpecification = freemarkerRenderer.write(model,
				getPlantUMLTemplateName(element) + ".ftl");
		plantUmlSpecification.ifPresent(file -> {
			Optional<File> renderedOutputFile = plantUMLRenderer.write(file);
			renderedOutputFile.ifPresent(render -> {
				final File resultingFile = new File(targetBaseDirectory,
						getOutputFileName(element) + getPointInTimeFileSuffix(keyPointInTime)
								+ fileFormats.get(contentOutputType).getFileSuffix());
				log.info("wrote diagram to file {}", resultingFile.getAbsolutePath());
				try {
					FileUtils.copyFile(render, resultingFile);
					renderedFiles.add(resultingFile);
					saveAssetAndRecordCdnUrl(resultingFile, element, townPlan, keyPointInTime);
				} catch (IOException e) {
					log.error(e);
				}
			});
		});
	}

	protected String getPointInTimeFileSuffix(@NonNull final KeyPointInTime pointInTime) {
		if (supportsTimeTravel()) {
			if (pointInTime.isToday())
				return "-as-is-today";
			if (pointInTime.isFuture())
				return "-to-be-" + pointInTime.getDate().format(DateTimeFormatter.ISO_LOCAL_DATE);
			if (pointInTime.isPast())
				return "-as-was-" + pointInTime.getDate().format(DateTimeFormatter.ISO_LOCAL_DATE);
		}
		return "";
	}

	protected String getPointInTimeTitleSuffix(@NonNull final KeyPointInTime pointInTime) {
		if (supportsTimeTravel()) {
			if (pointInTime.isToday())
				return " - AS IS: Today";
			if (pointInTime.isFuture())
				return " - TO BE: " + pointInTime.getTitle();
			if (pointInTime.isPast())
				return " - AS WAS: " + pointInTime.getTitle();
		}
		return "";
	}

	protected abstract String getContentDistributionTitle(T concept, ContentOutputType contentOutputType);

	private void saveAssetAndRecordCdnUrl(@NonNull File render, @NonNull final T element,
			@NonNull final TownPlan townPlan, @NonNull final KeyPointInTime keyPointInTime) {
		assetRepository.write(render, getOutputPath(element, keyPointInTime));
		assetRepository.getCdnUrl(getOutputPath(element, keyPointInTime)).ifPresent(cdnUrl -> {
			if (townPlan instanceof TownPlanImpl tpi) {
				tpi.execute(
						AddContentDistributionAspectCommand.builder().url(cdnUrl).modelComponentKey(element.getKey())
								.type(contentOutputType).title(getContentDistributionTitle(element, contentOutputType)
										+ getPointInTimeTitleSuffix(keyPointInTime))
								.build());
			}
		});
	}

	protected String getPlantUMLTemplateName(@NonNull final T element) {
		return element.getCategory();
	}

	protected String getOutputFileName(@NonNull final T element) {
		return element.getKey();
	}

	public String getOutputPath(@NonNull final T element, @NonNull final KeyPointInTime pointInTime) {
		return contentOutputType.getValue().toLowerCase() + "/" + folderName + "/" + getOutputFileName(element)
				+ getPointInTimeFileSuffix(pointInTime) + fileFormats.get(contentOutputType).getFileSuffix();
	}

	protected ConceptWriterModel<T> getModel(T element, KeyPointInTime pointInTime, @NonNull TownPlan townPlan) {
		return new ConceptWriterModel<>(element, pointInTime, townPlan);
	}
}
