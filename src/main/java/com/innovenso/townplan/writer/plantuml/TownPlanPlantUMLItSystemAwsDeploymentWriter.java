package com.innovenso.townplan.writer.plantuml;

import com.innovenso.townplan.api.KeyPointInTime;
import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.aspects.ContentOutputType;
import com.innovenso.townplan.api.value.it.ItSystem;
import com.innovenso.townplan.repository.AssetRepository;
import com.innovenso.townplan.writer.model.ConceptWriterModel;
import com.innovenso.townplan.writer.model.ITSystemAWSDeploymentDiagramModel;
import lombok.NonNull;

import java.io.File;

public class TownPlanPlantUMLItSystemAwsDeploymentWriter extends AbstractTownPlanPlantUMLConceptWriter<ItSystem> {

	public TownPlanPlantUMLItSystemAwsDeploymentWriter(@NonNull File targetBaseDirectory,
			@NonNull AssetRepository assetRepository, String folderName, @NonNull ContentOutputType contentOutputType) {
		super(targetBaseDirectory, assetRepository, folderName == null ? "system" : folderName, contentOutputType);
	}

	@Override
	public ConceptWriterModel<ItSystem> getModel(ItSystem concept, KeyPointInTime keyPointInTime,
			@NonNull TownPlan townPlan) {
		return new ITSystemAWSDeploymentDiagramModel(concept, keyPointInTime, townPlan);
	}

	@Override
	protected String getPlantUMLTemplateName(@NonNull ItSystem element) {
		return "system-aws-deployment";
	}

	@Override
	protected String getOutputFileName(@NonNull ItSystem element) {
		return super.getOutputFileName(element) + "-aws-deployment";
	}

	@Override
	protected String getContentDistributionTitle(ItSystem concept, ContentOutputType contentOutputType) {
		return "AWS Deployment Diagram (" + contentOutputType.getLabel() + ")";
	}
}
