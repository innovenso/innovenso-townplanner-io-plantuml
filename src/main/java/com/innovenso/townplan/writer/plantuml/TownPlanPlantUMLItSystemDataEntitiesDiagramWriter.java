package com.innovenso.townplan.writer.plantuml;

import com.innovenso.townplan.api.KeyPointInTime;
import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.aspects.ContentOutputType;
import com.innovenso.townplan.api.value.it.ItSystem;
import com.innovenso.townplan.repository.AssetRepository;
import com.innovenso.townplan.writer.model.ConceptWriterModel;
import com.innovenso.townplan.writer.model.SystemDataEntityWriterModel;
import lombok.NonNull;

import java.io.File;

public class TownPlanPlantUMLItSystemDataEntitiesDiagramWriter extends AbstractTownPlanPlantUMLConceptWriter<ItSystem> {

	public TownPlanPlantUMLItSystemDataEntitiesDiagramWriter(@NonNull File targetBaseDirectory,
			@NonNull AssetRepository assetRepository, String folderName, @NonNull ContentOutputType contentOutputType) {
		super(targetBaseDirectory, assetRepository, folderName == null ? "system" : folderName, contentOutputType);
	}

	@Override
	public ConceptWriterModel<ItSystem> getModel(ItSystem concept, KeyPointInTime keyPointInTime,
			@NonNull final TownPlan townPlan) {
		return new SystemDataEntityWriterModel(concept, keyPointInTime, townPlan);
	}

	@Override
	protected String getPlantUMLTemplateName(@NonNull ItSystem element) {
		return "system-entities";
	}

	@Override
	protected String getContentDistributionTitle(ItSystem concept, ContentOutputType contentOutputType) {
		return "Data Model (" + contentOutputType.getLabel() + ")";
	}
}
