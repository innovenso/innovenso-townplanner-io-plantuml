package com.innovenso.townplan.writer.plantuml;

import com.innovenso.townplan.api.value.aspects.ContentOutputType;
import com.innovenso.townplan.api.value.it.ItSystemIntegration;
import com.innovenso.townplan.repository.AssetRepository;
import lombok.NonNull;

import java.io.File;

public class TownPlanPlantUMLItSimpleSystemIntegrationWriter
		extends
			AbstractTownPlanPlantUMLConceptWriter<ItSystemIntegration> {

	public TownPlanPlantUMLItSimpleSystemIntegrationWriter(@NonNull File targetBaseDirectory,
			@NonNull AssetRepository assetRepository, String folderName, @NonNull ContentOutputType contentOutputType) {
		super(targetBaseDirectory, assetRepository, folderName == null ? "integration" : folderName, contentOutputType);
	}

	@Override
	protected String getPlantUMLTemplateName(@NonNull ItSystemIntegration element) {
		return "system-integration-simple";
	}

	@Override
	protected String getOutputFileName(@NonNull ItSystemIntegration element) {
		return super.getOutputFileName(element) + "-simple";
	}

	@Override
	protected String getContentDistributionTitle(ItSystemIntegration concept, ContentOutputType contentOutputType) {
		return "Simple Integration Diagram (" + contentOutputType.getLabel() + ")";
	}
}
