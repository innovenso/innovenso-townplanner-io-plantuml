package com.innovenso.townplan.writer.plantuml;

import com.innovenso.townplan.api.KeyPointInTime;
import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.aspects.ContentOutputType;
import com.innovenso.townplan.api.value.business.Enterprise;
import com.innovenso.townplan.repository.AssetRepository;
import com.innovenso.townplan.writer.model.ConceptWriterModel;
import com.innovenso.townplan.writer.model.EnterpriseTownPlanModel;
import lombok.NonNull;

import java.io.File;

public class TownPlanPlantUMLEnterpriseTownPlanWriter extends AbstractTownPlanPlantUMLConceptWriter<Enterprise> {

	public TownPlanPlantUMLEnterpriseTownPlanWriter(@NonNull File targetBaseDirectory,
			@NonNull AssetRepository assetRepository, String folderName, @NonNull ContentOutputType contentOutputType) {
		super(targetBaseDirectory, assetRepository, folderName == null ? "enterprise" : folderName, contentOutputType);
	}

	@Override
	public ConceptWriterModel<Enterprise> getModel(Enterprise concept, KeyPointInTime keyPointInTime,
			@NonNull final TownPlan townPlan) {
		return new EnterpriseTownPlanModel(concept, keyPointInTime, townPlan);
	}

	@Override
	protected String getPlantUMLTemplateName(@NonNull Enterprise element) {
		return "enterprise-town-plan";
	}

	@Override
	protected String getOutputFileName(@NonNull Enterprise element) {
		return super.getOutputFileName(element) + "-townplan";
	}

	@Override
	protected String getContentDistributionTitle(Enterprise concept, ContentOutputType contentOutputType) {
		return "Enterprise Town Plan (" + contentOutputType.getLabel() + ")";
	}
}
