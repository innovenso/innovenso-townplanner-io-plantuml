package com.innovenso.townplan.writer.plantuml;

import com.innovenso.townplan.api.KeyPointInTime;
import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.aspects.ContentOutputType;
import com.innovenso.townplan.api.value.it.ArchitectureBuildingBlock;
import com.innovenso.townplan.repository.AssetRepository;
import com.innovenso.townplan.writer.model.ArchitectureBuildingBlockContextDiagramModel;
import com.innovenso.townplan.writer.model.ConceptWriterModel;
import lombok.NonNull;

import java.io.File;

public class TownPlanPlantUMLArchitectureBuildingBlockContextDiagramWriter
		extends
			AbstractTownPlanPlantUMLConceptWriter<ArchitectureBuildingBlock> {

	public TownPlanPlantUMLArchitectureBuildingBlockContextDiagramWriter(@NonNull File targetBaseDirectory,
			@NonNull AssetRepository assetRepository, String folderName, @NonNull ContentOutputType contentOutputType) {
		super(targetBaseDirectory, assetRepository, folderName == null ? "architecture-building-block" : folderName,
				contentOutputType);
	}

	@Override
	public ConceptWriterModel<ArchitectureBuildingBlock> getModel(ArchitectureBuildingBlock concept,
			KeyPointInTime pointInTime, @NonNull TownPlan townPlan) {
		return new ArchitectureBuildingBlockContextDiagramModel(concept, pointInTime, townPlan);
	}

	@Override
	protected String getPlantUMLTemplateName(@NonNull ArchitectureBuildingBlock element) {
		return "architecture-building-block-context";
	}

	@Override
	protected String getOutputFileName(@NonNull ArchitectureBuildingBlock element) {
		return super.getOutputFileName(element) + "-context";
	}

	@Override
	protected String getContentDistributionTitle(ArchitectureBuildingBlock concept,
			ContentOutputType contentOutputType) {
		return "Context Diagram (" + contentOutputType.getLabel() + ")";
	}
}
