package com.innovenso.townplan.writer.plantuml;

import com.innovenso.townplan.api.value.aspects.ContentOutputType;
import com.innovenso.townplan.api.value.business.BusinessActor;
import com.innovenso.townplan.repository.AssetRepository;
import lombok.NonNull;

import java.io.File;

public class TownPlanPlantUMLBusinessActorWriter extends AbstractTownPlanPlantUMLConceptWriter<BusinessActor> {

	public TownPlanPlantUMLBusinessActorWriter(@NonNull File targetBaseDirectory,
			@NonNull AssetRepository assetRepository, String folderName, @NonNull ContentOutputType contentOutputType) {
		super(targetBaseDirectory, assetRepository, folderName == null ? "business-actor" : folderName,
				contentOutputType);
	}

	@Override
	protected String getPlantUMLTemplateName(@NonNull BusinessActor element) {
		return "business-actor";
	}

	@Override
	protected String getContentDistributionTitle(BusinessActor concept, ContentOutputType contentOutputType) {
		return "Business Actor Interaction Diagram (" + contentOutputType.getLabel() + ")";
	}
}
