package com.innovenso.townplan.writer.plantuml;

import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.view.View;

import java.io.File;
import java.util.List;

public interface TownPlanPlantUMLViewWriter<T extends View> {
	void write(TownPlan townPlan, T view, List<File> renderedFiles);
}
