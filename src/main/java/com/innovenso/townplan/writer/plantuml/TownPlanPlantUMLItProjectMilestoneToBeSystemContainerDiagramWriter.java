package com.innovenso.townplan.writer.plantuml;

import com.innovenso.townplan.api.KeyPointInTime;
import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.aspects.ContentOutputType;
import com.innovenso.townplan.api.value.it.ItProjectMilestone;
import com.innovenso.townplan.repository.AssetRepository;
import com.innovenso.townplan.writer.model.ConceptWriterModel;
import com.innovenso.townplan.writer.model.ProjectMilestoneSystemContainerDiagramModel;
import lombok.NonNull;

import java.io.File;

public class TownPlanPlantUMLItProjectMilestoneToBeSystemContainerDiagramWriter
		extends
			AbstractTownPlanPlantUMLConceptWriter<ItProjectMilestone> {

	public TownPlanPlantUMLItProjectMilestoneToBeSystemContainerDiagramWriter(@NonNull File targetBaseDirectory,
			@NonNull AssetRepository assetRepository, String folderName, @NonNull ContentOutputType contentOutputType) {
		super(targetBaseDirectory, assetRepository, folderName == null ? "project-milestone" : folderName,
				contentOutputType);
	}

	@Override
	protected String getPlantUMLTemplateName(@NonNull ItProjectMilestone element) {
		return "milestone-system-container-diagram";
	}

	@Override
	protected String getOutputFileName(@NonNull ItProjectMilestone element) {
		return super.getOutputFileName(element) + "-to-be";
	}

	@Override
	protected String getContentDistributionTitle(ItProjectMilestone concept, ContentOutputType contentOutputType) {
		return "To Be System Container Diagram (" + contentOutputType.getLabel() + ")";
	}

	@Override
	protected ConceptWriterModel<ItProjectMilestone> getModel(ItProjectMilestone element, KeyPointInTime keyPointInTime,
			@NonNull final TownPlan townPlan) {
		return new ProjectMilestoneSystemContainerDiagramModel(element, keyPointInTime, townPlan, true);
	}
}
