package com.innovenso.townplan.writer.plantuml;

import com.innovenso.townplan.api.KeyPointInTime;
import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.aspects.ContentOutputType;
import com.innovenso.townplan.api.value.business.Enterprise;
import com.innovenso.townplan.repository.AssetRepository;
import com.innovenso.townplan.writer.model.ConceptWriterModel;
import com.innovenso.townplan.writer.model.EnterpriseMindMapModel;
import lombok.NonNull;

import java.io.File;

public class TownPlanPlantUMLEnterpriseBusinessCapabilityMindMapWriter
		extends
			AbstractTownPlanPlantUMLConceptWriter<Enterprise> {

	public TownPlanPlantUMLEnterpriseBusinessCapabilityMindMapWriter(@NonNull File targetBaseDirectory,
			@NonNull AssetRepository assetRepository, String folderName, @NonNull ContentOutputType contentOutputType) {
		super(targetBaseDirectory, assetRepository, folderName == null ? "enterprise" : folderName, contentOutputType);
	}

	@Override
	public ConceptWriterModel<Enterprise> getModel(Enterprise concept, @NonNull final KeyPointInTime keyPointInTime,
			@NonNull final TownPlan townPlan) {
		return new EnterpriseMindMapModel(concept, keyPointInTime, townPlan);
	}

	@Override
	protected String getPlantUMLTemplateName(@NonNull Enterprise element) {
		return "enterprise-business-capability-mindmap";
	}

	@Override
	protected String getContentDistributionTitle(Enterprise concept, ContentOutputType contentOutputType) {
		return "Business Capability Map (" + contentOutputType.getLabel() + ")";
	}
}
