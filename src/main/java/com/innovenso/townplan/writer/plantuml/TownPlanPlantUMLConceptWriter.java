package com.innovenso.townplan.writer.plantuml;

import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.Concept;

import java.io.File;
import java.util.List;

public interface TownPlanPlantUMLConceptWriter<T extends Concept> {
	void write(TownPlan townPlan, T element, List<File> renderedFiles);

	default boolean supportsTimeTravel() {
		return false;
	}
}
