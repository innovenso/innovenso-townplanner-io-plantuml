package com.innovenso.townplan.writer.plantuml;

import com.innovenso.townplan.api.KeyPointInTime;
import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.aspects.ContentOutputType;
import com.innovenso.townplan.api.value.it.ArchitectureBuildingBlock;
import com.innovenso.townplan.repository.AssetRepository;
import com.innovenso.townplan.writer.model.ArchitectureBuildingBlockDiagramModel;
import com.innovenso.townplan.writer.model.ConceptWriterModel;
import lombok.NonNull;

import java.io.File;

public class TownPlanPlantUMLArchitectureBuildingBlockSimpleViewWriter
		extends
			AbstractTownPlanPlantUMLConceptWriter<ArchitectureBuildingBlock> {

	public TownPlanPlantUMLArchitectureBuildingBlockSimpleViewWriter(@NonNull File targetBaseDirectory,
			@NonNull AssetRepository assetRepository, String folderName, @NonNull ContentOutputType contentOutputType) {
		super(targetBaseDirectory, assetRepository, folderName == null ? "architecture-building-block" : folderName,
				contentOutputType);
	}

	@Override
	public ConceptWriterModel<ArchitectureBuildingBlock> getModel(ArchitectureBuildingBlock concept,
			KeyPointInTime keyPointInTime, @NonNull TownPlan townPlan) {
		return new ArchitectureBuildingBlockDiagramModel(concept, keyPointInTime, townPlan, false);
	}

	@Override
	protected String getPlantUMLTemplateName(@NonNull ArchitectureBuildingBlock element) {
		return "architecture-building-block-simple";
	}

	@Override
	protected String getOutputFileName(@NonNull ArchitectureBuildingBlock element) {
		return super.getOutputFileName(element) + "-simple";
	}

	@Override
	protected String getContentDistributionTitle(ArchitectureBuildingBlock concept,
			ContentOutputType contentOutputType) {
		return "Simple Architecture Building Block Diagram (" + contentOutputType.getLabel() + ")";
	}
}
