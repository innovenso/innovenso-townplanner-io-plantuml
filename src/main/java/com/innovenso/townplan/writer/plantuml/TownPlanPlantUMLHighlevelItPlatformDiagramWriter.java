package com.innovenso.townplan.writer.plantuml;

import com.innovenso.townplan.api.KeyPointInTime;
import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.aspects.ContentOutputType;
import com.innovenso.townplan.api.value.it.ItPlatform;
import com.innovenso.townplan.repository.AssetRepository;
import com.innovenso.townplan.writer.model.ConceptWriterModel;
import com.innovenso.townplan.writer.model.PlatformSystemDiagramModel;
import lombok.NonNull;

import java.io.File;

public class TownPlanPlantUMLHighlevelItPlatformDiagramWriter
		extends
			AbstractTownPlanPlantUMLConceptWriter<ItPlatform> {
	public TownPlanPlantUMLHighlevelItPlatformDiagramWriter(@NonNull File targetBaseDirectory,
			@NonNull AssetRepository assetRepository, String folderName, @NonNull ContentOutputType contentOutputType) {
		super(targetBaseDirectory, assetRepository, folderName == null ? "architecture-building-block" : folderName,
				contentOutputType);
	}

	@Override
	protected String getContentDistributionTitle(ItPlatform concept, ContentOutputType contentOutputType) {
		return "High Level Platform Diagram";
	}

	@Override
	protected String getPlantUMLTemplateName(@NonNull ItPlatform element) {
		return "platform-highlevel";
	}

	@Override
	public ConceptWriterModel<ItPlatform> getModel(ItPlatform element, KeyPointInTime pointInTime,
			@NonNull TownPlan townPlan) {
		return new PlatformSystemDiagramModel(element, pointInTime, townPlan);
	}

	@Override
	protected String getOutputFileName(@NonNull ItPlatform element) {
		return super.getOutputFileName(element) + "-highlevel";
	}

	@Override
	public boolean supportsTimeTravel() {
		return true;
	}
}
