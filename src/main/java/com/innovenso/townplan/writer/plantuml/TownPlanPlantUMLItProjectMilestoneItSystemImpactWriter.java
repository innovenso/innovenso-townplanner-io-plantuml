package com.innovenso.townplan.writer.plantuml;

import com.innovenso.townplan.api.value.aspects.ContentOutputType;
import com.innovenso.townplan.api.value.it.ItProjectMilestone;
import com.innovenso.townplan.repository.AssetRepository;
import lombok.NonNull;

import java.io.File;

public class TownPlanPlantUMLItProjectMilestoneItSystemImpactWriter
		extends
			AbstractTownPlanPlantUMLConceptWriter<ItProjectMilestone> {

	public TownPlanPlantUMLItProjectMilestoneItSystemImpactWriter(@NonNull File targetBaseDirectory,
			@NonNull AssetRepository assetRepository, String folderName, @NonNull ContentOutputType contentOutputType) {
		super(targetBaseDirectory, assetRepository, folderName == null ? "project-milestone" : folderName,
				contentOutputType);
	}

	@Override
	protected String getPlantUMLTemplateName(@NonNull ItProjectMilestone element) {
		return "milestone-systems";
	}

	@Override
	protected String getOutputFileName(@NonNull ItProjectMilestone element) {
		return super.getOutputFileName(element) + "-system-impact";
	}

	@Override
	protected String getContentDistributionTitle(ItProjectMilestone concept, ContentOutputType contentOutputType) {
		return "System Impact Diagram (" + contentOutputType.getLabel() + ")";
	}
}
