package com.innovenso.townplan.writer.plantuml;

import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.Concept;
import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.aspects.ContentOutputType;
import com.innovenso.townplan.api.value.business.BusinessActor;
import com.innovenso.townplan.api.value.business.Enterprise;
import com.innovenso.townplan.api.value.it.*;
import com.innovenso.townplan.api.value.strategy.BusinessCapability;
import com.innovenso.townplan.api.value.view.FlowView;
import com.innovenso.townplan.api.value.view.View;
import com.innovenso.townplan.repository.AssetRepository;
import com.innovenso.townplan.writer.pipeline.AbstractTownPlanWriter;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

import java.io.File;
import java.util.List;
import java.util.function.Consumer;

@Log4j2
public abstract class AbstractTownPlanPlantUMLWriter extends AbstractTownPlanWriter {
	private final TownPlanPlantUMLEnterpriseBusinessCapabilityMindMapWriter enterpriseBusinessCapabilityMindMapWriter;
	private final TownPlanPlantUMLEnterpriseTownPlanWriter enterpriseTownPlanWriter;
	private final TownPlanPlantUMLBusinessCapabilityWriter businessCapabilityWriter;
	private final TownPlanPlantUMLArchitectureBuildingBlockWriter architectureBuildingBlockWriter;
	private final TownPlanPlantUMLBusinessActorWriter businessActorWriter;
	private final TownPlanPlantUMLItSystemContainerDiagramWriter itSystemContainerDiagramWriter;
	private final TownPlanPlantUMLItSystemAwsDeploymentWriter awsDeploymentWriter;
	private final TownPlanPlantUMLItProjectMilestoneBuildingBlockImpactWriter milestoneBuildingBlockImpactWriter;
	private final TownPlanPlantUMLItProjectMilestoneBusinessCapabilityImpactWriter milestoneBusinessCapabilityImpactWriter;
	private final TownPlanPlantUMLItProjectMilestoneDataEntityImpactWriter milestoneDataEntityImpactWriter;
	private final TownPlanPlantUMLItProjectMilestoneItSystemImpactWriter milestoneItSystemImpactWriter;
	private final TownPlanPlantUMLItSystemIntegrationWriter systemIntegrationWriter;
	private final TownPlanPlantUMLItSystemToSystemIntegrationDiagramWriter systemToSystemIntegrationDiagramWriter;
	private final TownPlanPlantUMLItProjectMilestoneItSystemIntegrationImpactWriter milestoneItSystemIntegrationImpactWriter;
	private final TownPlanPlantUMLFlowViewWriter flowViewWriter;
	private final TownPlanPlantUMLFlowViewSequenceDiagramWriter flowViewSequenceDiagramWriter;
	private final TownPlanPlantUMLItSystemIntegrationRelationshipDiagramWriter systemIntegrationRelationshipDiagramWriter;
	private final TownPlanPlantUMLItSystemIntegrationSequenceDiagramWriter systemIntegrationSequenceDiagramWriter;
	private final TownPlanPlantUMLItSimpleSystemIntegrationWriter simpleSystemIntegrationWriter;
	private final TownPlanPlantUMLItProjectMilestoneIntegrationMapWriter milestoneIntegrationMapWriter;
	private final TownPlanPlantUMLArchitectureBuildingBlockContextDiagramWriter contextDiagramWriter;
	private final TownPlanPlantUMLArchitectureBuildingBlockSimpleViewWriter simpleBuildingBlockWriter;
	private final TownPlanPlantUMLItSystemContextDiagramWriter systemContextDiagramWriter;
	private final TownPlanPlantUMLEnterpriseIntegrationMapWriter enterpriseIntegrationMapWriter;
	private final TownPlanPlantUMLItProjectMilestoneAsIsSystemContainerDiagramWriter asIsWriter;
	private final TownPlanPlantUMLItProjectMilestoneToBeSystemContainerDiagramWriter toBeWriter;
	private final TownPlanPlantUMLHighlevelItPlatformDiagramWriter platformWriter;
	private final TownPlanPlantUMLDataEntityWriter entityWriter;
	private final TownPlanPlantUMLItSystemDataEntitiesDiagramWriter systemEntityWriter;
	private final File targetBaseDirectory;

	public AbstractTownPlanPlantUMLWriter(@NonNull final File targetBaseDirectory,
			@NonNull final AssetRepository assetRepository, final String folderName,
			@NonNull ContentOutputType contentOutputType) {
		super(assetRepository, contentOutputType);
		this.targetBaseDirectory = targetBaseDirectory;
		this.enterpriseBusinessCapabilityMindMapWriter = new TownPlanPlantUMLEnterpriseBusinessCapabilityMindMapWriter(
				targetBaseDirectory, assetRepository, folderName, contentOutputType);
		this.enterpriseTownPlanWriter = new TownPlanPlantUMLEnterpriseTownPlanWriter(targetBaseDirectory,
				assetRepository, folderName, contentOutputType);
		this.businessCapabilityWriter = new TownPlanPlantUMLBusinessCapabilityWriter(targetBaseDirectory,
				assetRepository, folderName, contentOutputType);
		this.architectureBuildingBlockWriter = new TownPlanPlantUMLArchitectureBuildingBlockWriter(targetBaseDirectory,
				assetRepository, folderName, contentOutputType);
		this.businessActorWriter = new TownPlanPlantUMLBusinessActorWriter(targetBaseDirectory, assetRepository,
				folderName, contentOutputType);
		this.itSystemContainerDiagramWriter = new TownPlanPlantUMLItSystemContainerDiagramWriter(targetBaseDirectory,
				assetRepository, folderName, contentOutputType);
		this.awsDeploymentWriter = new TownPlanPlantUMLItSystemAwsDeploymentWriter(targetBaseDirectory, assetRepository,
				folderName, contentOutputType);
		this.milestoneBuildingBlockImpactWriter = new TownPlanPlantUMLItProjectMilestoneBuildingBlockImpactWriter(
				targetBaseDirectory, assetRepository, folderName, contentOutputType);
		this.milestoneBusinessCapabilityImpactWriter = new TownPlanPlantUMLItProjectMilestoneBusinessCapabilityImpactWriter(
				targetBaseDirectory, assetRepository, folderName, contentOutputType);
		this.milestoneDataEntityImpactWriter = new TownPlanPlantUMLItProjectMilestoneDataEntityImpactWriter(
				targetBaseDirectory, assetRepository, folderName, contentOutputType);
		this.milestoneItSystemImpactWriter = new TownPlanPlantUMLItProjectMilestoneItSystemImpactWriter(
				targetBaseDirectory, assetRepository, folderName, contentOutputType);
		this.systemIntegrationWriter = new TownPlanPlantUMLItSystemIntegrationWriter(targetBaseDirectory,
				assetRepository, folderName, contentOutputType);
		this.systemToSystemIntegrationDiagramWriter = new TownPlanPlantUMLItSystemToSystemIntegrationDiagramWriter(
				targetBaseDirectory, assetRepository, folderName, contentOutputType);
		this.milestoneItSystemIntegrationImpactWriter = new TownPlanPlantUMLItProjectMilestoneItSystemIntegrationImpactWriter(
				targetBaseDirectory, assetRepository, folderName, contentOutputType);
		this.flowViewWriter = new TownPlanPlantUMLFlowViewWriter(targetBaseDirectory, assetRepository, folderName,
				contentOutputType);
		this.flowViewSequenceDiagramWriter = new TownPlanPlantUMLFlowViewSequenceDiagramWriter(targetBaseDirectory,
				assetRepository, folderName, contentOutputType);
		this.systemIntegrationRelationshipDiagramWriter = new TownPlanPlantUMLItSystemIntegrationRelationshipDiagramWriter(
				targetBaseDirectory, assetRepository, folderName, contentOutputType);
		this.systemIntegrationSequenceDiagramWriter = new TownPlanPlantUMLItSystemIntegrationSequenceDiagramWriter(
				targetBaseDirectory, assetRepository, folderName, contentOutputType);
		this.milestoneIntegrationMapWriter = new TownPlanPlantUMLItProjectMilestoneIntegrationMapWriter(
				targetBaseDirectory, assetRepository, folderName, contentOutputType);
		this.simpleSystemIntegrationWriter = new TownPlanPlantUMLItSimpleSystemIntegrationWriter(targetBaseDirectory,
				assetRepository, folderName, contentOutputType);
		this.contextDiagramWriter = new TownPlanPlantUMLArchitectureBuildingBlockContextDiagramWriter(
				targetBaseDirectory, assetRepository, folderName, contentOutputType);
		this.simpleBuildingBlockWriter = new TownPlanPlantUMLArchitectureBuildingBlockSimpleViewWriter(
				targetBaseDirectory, assetRepository, folderName, contentOutputType);
		this.systemContextDiagramWriter = new TownPlanPlantUMLItSystemContextDiagramWriter(targetBaseDirectory,
				assetRepository, folderName, contentOutputType);
		this.enterpriseIntegrationMapWriter = new TownPlanPlantUMLEnterpriseIntegrationMapWriter(targetBaseDirectory,
				assetRepository, folderName, contentOutputType);
		this.asIsWriter = new TownPlanPlantUMLItProjectMilestoneAsIsSystemContainerDiagramWriter(targetBaseDirectory,
				assetRepository, folderName, contentOutputType);
		this.toBeWriter = new TownPlanPlantUMLItProjectMilestoneToBeSystemContainerDiagramWriter(targetBaseDirectory,
				assetRepository, folderName, contentOutputType);
		this.platformWriter = new TownPlanPlantUMLHighlevelItPlatformDiagramWriter(targetBaseDirectory, assetRepository,
				folderName, contentOutputType);
		this.entityWriter = new TownPlanPlantUMLDataEntityWriter(targetBaseDirectory, assetRepository, folderName,
				contentOutputType);
		this.systemEntityWriter = new TownPlanPlantUMLItSystemDataEntitiesDiagramWriter(targetBaseDirectory,
				assetRepository, folderName, contentOutputType);
	}

	@Override
	protected void writeFiles(@NonNull final TownPlan townPlan, @NonNull final List<File> renderedFiles) {
		final List<Class<? extends Element>> conceptClassesToRender = List.of(Enterprise.class,
				BusinessCapability.class, ArchitectureBuildingBlock.class, BusinessActor.class, ItPlatform.class,
				ItSystem.class, ItProjectMilestone.class, ItSystemIntegration.class, DataEntity.class);
		conceptClassesToRender.forEach(conceptClass -> townPlan.getElements(conceptClass)
				.forEach(element -> this.writeFiles(townPlan, element, renderedFiles)));
		townPlan.getFlowViews().forEach(flowView -> this.writeFiles(townPlan, flowView, renderedFiles));
	}

	@Override
	protected void writeFiles(@NonNull final TownPlan townPlan, @NonNull final List<File> renderedFiles,
			@NonNull final String conceptKey) {
		townPlan.getConcept(conceptKey).ifPresent(concept -> this.writeFiles(townPlan, concept, renderedFiles));
		townPlan.getFlowView(conceptKey).ifPresent(view -> this.writeFiles(townPlan, view, renderedFiles));
	}

	private Consumer<View> viewWriter(@NonNull final TownPlan townPlan, @NonNull final List<File> renderedFiles) {
		return view -> {
			if (view instanceof FlowView flowView) {
				flowViewWriter.write(townPlan, flowView, renderedFiles);
				flowViewSequenceDiagramWriter.write(townPlan, flowView, renderedFiles);
			}
		};
	}

	private Consumer<Concept> enterpriseWriter(@NonNull final TownPlan townPlan,
			@NonNull final List<File> renderedFiles) {
		return concept -> {
			if (concept instanceof Enterprise enterprise) {
				enterpriseBusinessCapabilityMindMapWriter.write(townPlan, enterprise, renderedFiles);
				enterpriseTownPlanWriter.write(townPlan, enterprise, renderedFiles);
				enterpriseIntegrationMapWriter.write(townPlan, enterprise, renderedFiles);
			}
		};
	}

	private Consumer<Concept> platformWriter(@NonNull final TownPlan townPlan,
			@NonNull final List<File> renderedFiles) {
		return concept -> {
			if (concept instanceof ItPlatform platform) {
				platformWriter.write(townPlan, platform, renderedFiles);
			}
		};
	}

	private Consumer<Concept> entityWriter(@NonNull final TownPlan townPlan, @NonNull final List<File> renderedFiles) {
		return concept -> {
			if (concept instanceof DataEntity entity) {
				entityWriter.write(townPlan, entity, renderedFiles);
			}
		};
	}

	private Consumer<Concept> capabilityWriter(@NonNull final TownPlan townPlan,
			@NonNull final List<File> renderedFiles) {
		return concept -> {
			if (concept instanceof BusinessCapability capability) {
				businessCapabilityWriter.write(townPlan, capability, renderedFiles);
			}
		};
	}

	private Consumer<Concept> buildingBlockWriter(@NonNull final TownPlan townPlan,
			@NonNull final List<File> renderedFiles) {
		return concept -> {
			if (concept instanceof ArchitectureBuildingBlock block) {
				architectureBuildingBlockWriter.write(townPlan, block, renderedFiles);
				contextDiagramWriter.write(townPlan, block, renderedFiles);
				simpleBuildingBlockWriter.write(townPlan, block, renderedFiles);
			}
		};
	}

	private Consumer<Concept> actorWriter(@NonNull final TownPlan townPlan, @NonNull final List<File> renderedFiles) {
		return concept -> {
			if (concept instanceof BusinessActor actor) {
				businessActorWriter.write(townPlan, actor, renderedFiles);
			}
		};
	}

	private Consumer<Concept> systemWriter(@NonNull final TownPlan townPlan, @NonNull final List<File> renderedFiles) {
		return concept -> {
			if (concept instanceof ItSystem system) {
				itSystemContainerDiagramWriter.write(townPlan, system, renderedFiles);
				awsDeploymentWriter.write(townPlan, system, renderedFiles);
				systemToSystemIntegrationDiagramWriter.write(townPlan, system, renderedFiles);
				systemContextDiagramWriter.write(townPlan, system, renderedFiles);
				systemEntityWriter.write(townPlan, system, renderedFiles);
			}
		};
	}

	private Consumer<Concept> systemIntegrationWriter(@NonNull final TownPlan townPlan,
			@NonNull final List<File> renderedFiles) {
		return concept -> {
			if (concept instanceof ItSystemIntegration integration) {
				systemIntegrationWriter.write(townPlan, integration, renderedFiles);
				systemIntegrationRelationshipDiagramWriter.write(townPlan, integration, renderedFiles);
				systemIntegrationSequenceDiagramWriter.write(townPlan, integration, renderedFiles);
				simpleSystemIntegrationWriter.write(townPlan, integration, renderedFiles);
			}
		};
	}

	private Consumer<Concept> milestoneWriter(@NonNull final TownPlan townPlan,
			@NonNull final List<File> renderedFiles) {
		return concept -> {
			if (concept instanceof ItProjectMilestone milestone) {
				milestoneBuildingBlockImpactWriter.write(townPlan, milestone, renderedFiles);
				milestoneBusinessCapabilityImpactWriter.write(townPlan, milestone, renderedFiles);
				milestoneDataEntityImpactWriter.write(townPlan, milestone, renderedFiles);
				milestoneItSystemImpactWriter.write(townPlan, milestone, renderedFiles);
				milestoneItSystemIntegrationImpactWriter.write(townPlan, milestone, renderedFiles);
				milestoneIntegrationMapWriter.write(townPlan, milestone, renderedFiles);
				asIsWriter.write(townPlan, milestone, renderedFiles);
				toBeWriter.write(townPlan, milestone, renderedFiles);
			}
		};
	}

	public void writeFiles(@NonNull final TownPlan townPlan, @NonNull final Concept concept,
			@NonNull List<File> renderedFiles) {
		final List<Consumer<Concept>> writeFunctions = List.of(enterpriseWriter(townPlan, renderedFiles),
				capabilityWriter(townPlan, renderedFiles), actorWriter(townPlan, renderedFiles),
				buildingBlockWriter(townPlan, renderedFiles), systemWriter(townPlan, renderedFiles),
				milestoneWriter(townPlan, renderedFiles), systemIntegrationWriter(townPlan, renderedFiles),
				platformWriter(townPlan, renderedFiles), entityWriter(townPlan, renderedFiles));
		writeFunctions.forEach(writeFunction -> writeFunction.accept(concept));
	}

	public void writeFiles(@NonNull final TownPlan townPlan, @NonNull final FlowView flowView,
			@NonNull final List<File> renderedFiles) {
		viewWriter(townPlan, renderedFiles).accept(flowView);
	}

	@Override
	public File getTargetBaseDirectory() {
		return targetBaseDirectory;
	}

	@Override
	public void setTargetBaseDirectory(@NonNull File targetBaseDirectory) {
	}
}
