package com.innovenso.townplan.writer.plantuml;

import com.innovenso.townplan.api.KeyPointInTime;
import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.aspects.ContentOutputType;
import com.innovenso.townplan.api.value.strategy.BusinessCapability;
import com.innovenso.townplan.repository.AssetRepository;
import com.innovenso.townplan.writer.model.BusinessCapabilityWriterModel;
import com.innovenso.townplan.writer.model.ConceptWriterModel;
import lombok.NonNull;

import java.io.File;

public class TownPlanPlantUMLBusinessCapabilityWriter
		extends
			AbstractTownPlanPlantUMLConceptWriter<BusinessCapability> {

	public TownPlanPlantUMLBusinessCapabilityWriter(@NonNull File targetBaseDirectory,
			@NonNull AssetRepository assetRepository, String folderName, @NonNull ContentOutputType contentOutputType) {
		super(targetBaseDirectory, assetRepository, folderName == null ? "business-capability" : folderName,
				contentOutputType);
	}

	@Override
	public ConceptWriterModel<BusinessCapability> getModel(BusinessCapability concept,
			@NonNull final KeyPointInTime keyPointInTime, @NonNull TownPlan townPlan) {
		return new BusinessCapabilityWriterModel(concept, keyPointInTime, townPlan);
	}

	@Override
	protected String getPlantUMLTemplateName(@NonNull BusinessCapability element) {
		return "business-capability";
	}

	@Override
	protected String getContentDistributionTitle(BusinessCapability concept, ContentOutputType contentOutputType) {
		return "Business Capability Hierarchy Diagram (" + contentOutputType.getLabel() + ")";
	}
}
