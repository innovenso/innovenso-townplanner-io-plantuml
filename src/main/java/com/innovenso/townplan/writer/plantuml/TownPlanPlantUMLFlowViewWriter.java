package com.innovenso.townplan.writer.plantuml;

import com.innovenso.townplan.api.value.aspects.ContentOutputType;
import com.innovenso.townplan.api.value.view.FlowView;
import com.innovenso.townplan.repository.AssetRepository;
import com.innovenso.townplan.writer.model.FlowViewWriterModel;
import com.innovenso.townplan.writer.model.ViewWriterModel;
import lombok.NonNull;

import java.io.File;

public class TownPlanPlantUMLFlowViewWriter extends AbstractTownPlanPlantUMLViewWriter<FlowView> {

	public TownPlanPlantUMLFlowViewWriter(@NonNull File targetBaseDirectory, @NonNull AssetRepository assetRepository,
			String folderName, @NonNull ContentOutputType contentOutputType) {
		super(targetBaseDirectory, assetRepository, folderName == null ? "view" : folderName, contentOutputType);
	}

	@Override
	protected String getContentDistributionTitle(FlowView view, ContentOutputType contentOutputType) {
		return "Flow diagram (" + contentOutputType.getLabel() + ")";
	}

	@Override
	protected String getPlantUMLTemplateName(@NonNull FlowView view) {
		return "flow-view";
	}

	@Override
	protected ViewWriterModel<FlowView> getModel(FlowView view) {
		return new FlowViewWriterModel(view);
	}
}
