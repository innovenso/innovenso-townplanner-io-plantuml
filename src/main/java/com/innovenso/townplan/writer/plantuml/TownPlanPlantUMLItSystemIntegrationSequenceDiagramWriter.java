package com.innovenso.townplan.writer.plantuml;

import com.innovenso.townplan.api.KeyPointInTime;
import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.aspects.ContentOutputType;
import com.innovenso.townplan.api.value.it.ItSystemIntegration;
import com.innovenso.townplan.repository.AssetRepository;
import com.innovenso.townplan.writer.model.ConceptWriterModel;
import com.innovenso.townplan.writer.model.ITSystemIntegrationRelationshipDiagramModel;
import lombok.NonNull;

import java.io.File;

public class TownPlanPlantUMLItSystemIntegrationSequenceDiagramWriter
		extends
			AbstractTownPlanPlantUMLConceptWriter<ItSystemIntegration> {

	public TownPlanPlantUMLItSystemIntegrationSequenceDiagramWriter(@NonNull File targetBaseDirectory,
			@NonNull AssetRepository assetRepository, String folderName, @NonNull ContentOutputType contentOutputType) {
		super(targetBaseDirectory, assetRepository, folderName == null ? "integration" : folderName, contentOutputType);
	}

	@Override
	protected String getPlantUMLTemplateName(@NonNull ItSystemIntegration element) {
		return "system-integration-sequence-view";
	}

	@Override
	protected String getOutputFileName(@NonNull ItSystemIntegration element) {
		return super.getOutputFileName(element) + "-sequence";
	}

	@Override
	protected ConceptWriterModel<ItSystemIntegration> getModel(ItSystemIntegration element,
			KeyPointInTime keyPointInTime, @NonNull final TownPlan townPlan) {
		return new ITSystemIntegrationRelationshipDiagramModel(element, keyPointInTime, townPlan);
	}

	@Override
	protected String getContentDistributionTitle(ItSystemIntegration concept, ContentOutputType contentOutputType) {
		return "Sequence Diagram (" + contentOutputType.getLabel() + ")";
	}
}
