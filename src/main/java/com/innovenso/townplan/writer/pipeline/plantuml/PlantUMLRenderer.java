package com.innovenso.townplan.writer.pipeline.plantuml;

import lombok.NonNull;
import net.sourceforge.plantuml.FileFormat;
import net.sourceforge.plantuml.FileFormatOption;
import net.sourceforge.plantuml.SourceStringReader;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Optional;
import java.util.UUID;

public record PlantUMLRenderer(@NonNull FileFormat fileFormat) {

	public Optional<File> write(@NonNull final File specificationFile) {
		try {
			final File outputFile = File.createTempFile(UUID.randomUUID().toString(), fileFormat.getFileSuffix());
			final String specification = FileUtils.readFileToString(specificationFile, "UTF-8");
			final SourceStringReader reader = new SourceStringReader(specification);
			try (final OutputStream outputStream = new FileOutputStream(outputFile)) {
				reader.generateImage(outputStream, new FileFormatOption(fileFormat));
			}

			return Optional.of(outputFile);
		} catch (IOException e) {
			return Optional.empty();
		}
	}
}
