@startuml
<#include "lib/archimate.ftl">
<#import  "lib/system.ftl" as s>
<#import  "lib/integration.ftl" as i>
<#import  "lib/relationship.ftl" as r>


    <#list element.systems as system>
        System(${system.key}, "${system.title?replace('"','')?replace('\n',' ')}", "${system.description?replace('"','')?replace('\n',' ')}", "UNKNOWN")
    </#list>

    <#list element.relationships as relationship>
        Rel_Access(${relationship.source.key}, ${relationship.target.key}, "${relationship.label?replace('"','')?replace('\n',' ')}")
    </#list>

@enduml