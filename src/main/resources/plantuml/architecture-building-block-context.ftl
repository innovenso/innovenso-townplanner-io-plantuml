@startuml
<#include "lib/archimate.ftl">
<#import  "lib/building-block.ftl" as bb>
<#import  "lib/actor.ftl" as a>
<#import "lib/relationship.ftl" as r>

<@bb.application_building_block block=element.centralBuildingBlock />

<#-- render all the building blocks that interact with the central building block -->
    <#list element.buildingBlocks as buildingBlock>
        <@bb.application_building_block block=buildingBlock />
    </#list>

<#-- render all the actors that interact with the system itself or any of the containers inside this system -->
    <#list element.actors as actor>
        <@a.business_actor actor=actor />
    </#list>

<#-- render all relationships -->
    <#list element.relationships as relationship>
        <@r.element_relationship relationship=relationship title=relationship.label />
    </#list>

@enduml