@startuml
<#include "lib/archimate.ftl">
<#import  "lib/system.ftl" as s>
<#import  "lib/integration.ftl" as i>

<@i.system_integration integration=element.concept />

<@s.application_system system=element.concept.sourceSystem />
${element.concept.sourceSystem.key} -l- ${element.key}

<@s.application_system system=element.concept.targetSystem />
${element.concept.targetSystem.key} -r- ${element.key}

@enduml