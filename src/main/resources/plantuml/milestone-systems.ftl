@startmindmap
!includeurl https://raw.githubusercontent.com/ebbypeter/Archimate-PlantUML/master/Archimate.puml

sprite $aSystem jar:archimate/application-component
sprite $aMilestone jar:archimate/implementation-plateau
sprite $aProject jar:archimate/implementation-workpackage

*[#IMPLEMENTATION]:  <$aProject> <b>${element.concept.project.title}</b>

${element.concept.project.getDescriptionWrapped(60)};

**[#IMPLEMENTATION]: <$aMilestone> <b>${element.title}</b>

${element.concept.getDescriptionWrapped(60)};

<#if element.concept.getAddedItSystems()?has_content>
***[#GREEN] <&plus>
<#list element.concept.getAddedItSystems() as cap>
    ****[#APPLICATION]: <$aSystem> <b>${cap.title}</b>

    ${cap.getDescriptionWrapped(60)};
</#list>
</#if>

<#if element.concept.getRemovedItSystems()?has_content>
***[#RED] <&minus>
<#list element.concept.getRemovedItSystems() as cap>
    ****[#APPLICATION]: <$aSystem> <b>${cap.title}</b>

    ${cap.getDescriptionWrapped(60)};
</#list>
</#if>

<#if element.concept.getChangedItSystems()?has_content>
***[#YELLOW] <&pencil>
<#list element.concept.getChangedItSystems() as cap>
    ****[#APPLICATION]: <$aSystem> <b>${cap.title}</b>

    ${cap.getDescriptionWrapped(60)};
</#list>
</#if>

@endmindmap