@startuml
<#include "lib/aws.ftl">

<#list element.regions as region>
AwsRegion(${region.key}, "${region.title}") {

    <#list element.getVpcs(region.key) as vpc>
        AwsVPC(${vpc.key}, "${vpc.title}", "${vpc.networkMask}", "${vpc.description}") {

        AwsRouter("${vpc.key}_router") {
            <#list element.getRouteTables(vpc.key) as rt>
                AwsRouteTable("${rt.key}", "${rt.title}", "${rt.description}")
            </#list>
        }

        <#list element.getAutoscalingGroups(vpc.key) as asg>
            AwsAutoScalingGroup("${asg.key}", "${asg.title}", "${asg.description}")
        </#list>

        <#list element.getVpcInstances(vpc.key) as instance>
            AwsInstance(${instance.key}, "${instance.title}", "${instance.instanceType}", "${element.getInstanceLogo(instance.instanceType)}", "${instance.description}")
        </#list>

        <#list vpc.availabilityZones as az>
            AwsAZ(${az.key}, "${az.title}") {
            <#list element.getSubnets(vpc.key, az.key) as subnet>
                AwsSubnet(${subnet.key}, "${subnet.title}", "${subnet.subnetMask}") {
                <#list element.getNonEmptySecurityGroups(subnet.key) as securityGroup>
                    AwsSecurityGroup("${subnet.key}_${securityGroup.key}", "${securityGroup.title}", "${securityGroup.description}")
                </#list>
                <#list element.getInstances(subnet.key) as instance>
                    AwsInstance("${instance.key}", "${instance.title}", "${instance.instanceType}", "${element.getInstanceLogo(instance.instanceType)}", "${instance.description}")
                    <#list instance.securityGroups as instanceSecurityGroup>
                        ${instance.key} ..> ${subnet.key}_${instanceSecurityGroup.key}
                    </#list>
                </#list>

                }
            </#list>
            }
        </#list>
        }
    </#list>

    <#list element.getRegionalInstances(region.key) as instance>
        AwsInstance(${instance.key}, "${instance.title}", "${instance.instanceType}", "${element.getInstanceLogo(instance.instanceType)}", "${instance.description}")
    </#list>

    <#list element.getIpAddresses(region.key) as ipAddress>
        AwsEip(${ipAddress.key}, "${ipAddress.title}", "${ipAddress.description}")
    </#list>

}
</#list>
<#list element.getGlobalInstances() as instance>
    AwsInstance(${instance.key}, "${instance.title}", "${instance.instanceType}", "${element.getInstanceLogo(instance.instanceType)}", "${instance.description}")
</#list>

<#list element.relationships as relationship>
    ${relationship.left} --> ${relationship.right}: ${relationship.middle}
</#list>

<#list element.dependencies as dependency>
    ${dependency.source.key} --> ${dependency.target.key}: ${dependency.title}
</#list>
@enduml