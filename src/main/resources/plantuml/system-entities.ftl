@startuml
<#import "lib/entity.ftl" as d>

skinparam class {
    BackgroundColor White
    ArrowColor Black
    BorderColor Black
    <#list element.entityTypes as entityType>
        BackgroundColor<<${entityType.label}>> ${element.getColor(entityType)}
    </#list>
}

<@d.entity_group entityList=element.domainModel title="Domain Model" />
<@d.entity_group entityList=element.events title="Events" />
<@d.entity_group entityList=element.commands title="Commands" />
<@d.entity_group entityList=element.readModels title="Read Models" />
<@d.entity_group entityList=element.queries title="Queries" />

    <#list element.hasOnes as relationship>
        ${relationship.source.key} "1" -- "1" ${relationship.target.key}
    </#list>

    <#list element.hasZeroOrOnes as relationship>
        ${relationship.source.key} "1" -- "0..1" ${relationship.target.key}
    </#list>

    <#list element.hasZeroOrMores as relationship>
        ${relationship.source.key} "1" -- "*" ${relationship.target.key}
    </#list>

    <#list element.hasOneOrMores as relationship>
        ${relationship.source.key} "1" -- "1..*" ${relationship.target.key}
    </#list>

<#list element.entityTypes as entityType>
    hide <<${entityType.label}>> circle
</#list>

@enduml


