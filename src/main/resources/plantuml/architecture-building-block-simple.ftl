@startuml
<#include "lib/archimate.ftl">
<#import  "lib/system.ftl" as s>
<#import "lib/relationship.ftl" as r>
<#import "lib/building-block.ftl" as b>

<@b.application_building_block block=element.concept />

<#list element.enterprises as enterprise>
    Physical_Facility(${enterprise.key}, "${enterprise.title}")
</#list>

<#list element.businessCapabilities as capability>
    Strategy_Capability(${capability.key}, "${capability.title}")
</#list>

<#list element.systems as system>
    <@s.application_system system=system />
</#list>

<#list element.relationships as relationship>
    <@r.element_relationship relationship=relationship title=relationship.title />
</#list>

@enduml