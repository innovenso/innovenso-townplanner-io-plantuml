@startuml
<#include "lib/archimate.ftl">
<#import  "lib/system.ftl" as s>
<#import  "lib/container.ftl" as c>
<#import "lib/relationship.ftl" as r>
<#import "lib/building-block.ftl" as b>

Physical_Facility(${element.key}, "${element.title}")

<#list element.businessCapabilities as capability>
    <#if capability.level gt 2>
        Business_Function(${capability.key}, "${capability.title}")
    <#else>
        Strategy_Capability(${capability.key}, "${capability.title}")
    </#if>
</#list>

<#list element.buildingBlocks as buildingBlock>
    <@b.application_building_block block=buildingBlock />
</#list>

<#list element.systems as system>
    <@s.application_system system=system />
</#list>

<#list element.containers as container>
    <@c.application_container container=container />
</#list>

<#list element.relationships as relationship>
    <@r.element_relationship relationship=relationship title=relationship.title />
</#list>


@enduml