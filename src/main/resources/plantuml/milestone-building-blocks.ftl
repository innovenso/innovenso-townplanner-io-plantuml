@startmindmap
!includeurl https://raw.githubusercontent.com/ebbypeter/Archimate-PlantUML/master/Archimate.puml

sprite $aBuildingBlock jar:archimate/application-function
sprite $aMilestone jar:archimate/implementation-plateau
sprite $aProject jar:archimate/implementation-workpackage

*[#IMPLEMENTATION]:  <$aProject> <b>${element.concept.project.title}</b>

${element.concept.project.getDescriptionWrapped(60)};

**[#IMPLEMENTATION]: <$aMilestone> <b>${element.title}</b>

${element.concept.getDescriptionWrapped(60)};

<#if element.concept.getAddedBuildingBlocks()?has_content>
***[#GREEN] <&plus>
<#list element.concept.getAddedBuildingBlocks() as cap>
    ****[#APPLICATION]: <$aBuildingBlock> <b>${cap.title}</b>

    ${cap.getDescriptionWrapped(60)};
</#list>
</#if>

<#if element.concept.getRemovedBuildingBlocks()?has_content>
***[#RED] <&minus>
<#list element.concept.getRemovedBuildingBlocks() as cap>
    ****[#APPLICATION]: <$aBuildingBlock> <b>${cap.title}</b>

    ${cap.getDescriptionWrapped(60)};
</#list>
</#if>

<#if element.concept.getChangedBuildingBlocks()?has_content>
***[#YELLOW] <&pencil>
<#list element.concept.getChangedBuildingBlocks() as cap>
    ****[#APPLICATION]: <$aBuildingBlock> <b>${cap.title}</b>

    ${cap.getDescriptionWrapped(60)};
</#list>
</#if>

@endmindmap