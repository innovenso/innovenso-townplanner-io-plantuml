@startuml
<#include "lib/archimate.ftl">
<#import "lib/business-capability.ftl" as cap>

Business_Capability_Map_Group(${element.key}, "${element.title}"){
    <#list element.children as child>
        <@cap.business_capability capability=child />
    </#list>
}

@enduml