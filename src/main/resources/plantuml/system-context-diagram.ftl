@startuml
<#include "lib/archimate.ftl">
<#import  "lib/container.ftl" as c>
<#import  "lib/system.ftl" as s>
<#import  "lib/actor.ftl" as a>
<#import "lib/relationship.ftl" as r>

<#list element.businessCapabilities as cap>
    Business_Function_Group(${cap.key}, "${cap.title}") {
        <#list element.getSystems(cap.key) as system>
            <@s.simple_system system=system />
        </#list>
    }
</#list>

<#list element.systemsWithoutCapabilities as system>
    <@s.simple_system system=system />
</#list>

<#-- render all the actors that interact with the system itself or any of the containers inside this system -->
    <#list element.actors as actor>
        <@a.business_actor actor=actor />
    </#list>

<#-- render all relationships -->
    <#list element.flows as relationship>
        <@r.element_relationship relationship=relationship title=relationship.label />
    </#list>

@enduml