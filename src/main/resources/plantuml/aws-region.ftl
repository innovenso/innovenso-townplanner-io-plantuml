@startuml
<#include "lib/aws.ftl">

AwsRegion(${element.key}, "${element.title}") {
<#list element.concept.vpcs as vpc>
    AwsVPC(${vpc.key}, "${vpc.title}", "${vpc.networkMask}", "${vpc.description}") {
        <#list vpc.availabilityZones as az>
            AwsAZ(${az.key}, "${az.title}") {
            <#list element.getSubnets(vpc.key, az.key) as subnet>
                AwsSubnet(${subnet.key}, "${subnet.title}", "${subnet.subnetMask}") {
                    <#list element.getNonEmptySecurityGroups(vpc.key, subnet.key) as securityGroup>
                        AwsSecurityGroup(${securityGroup.key}, "${securityGroup.title}")
                    </#list>
                }
            </#list>
            }
        </#list>
    }
</#list>
}

@enduml