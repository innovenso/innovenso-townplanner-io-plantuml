@startuml
autonumber
skinparam monochrome true
<#list element.steps as step>
<#if step.response>
"${step.relationship.target.title}" as ${step.relationship.target.key} -> "${step.relationship.source.title}" as ${step.relationship.source.key}: "${step.actualTitle}"
<#else>
"${step.relationship.source.title}" as ${step.relationship.source.key} -> "${step.relationship.target.title}" as ${step.relationship.target.key}: "${step.actualTitle}"
</#if>
</#list>

@enduml