@startuml

skinparam class {
    BackgroundColor White
    ArrowColor Black
    BorderColor Black
    <#list element.entityTypes as entityType>
        BackgroundColor<<${entityType.label}>> ${element.getColor(entityType)}
    </#list>
}



<#list element.entities as entity>
        class "${entity.title}" as ${entity.camelCasedKey} <<${entity.entityType.label}>>{
            <#list entity.fields as field>
                + ${field.title} : ${field.type} [${field.fieldConstraints}]
            </#list>
        }
    </#list>


    <#list element.hasOnes as relationship>
        ${relationship.source.camelCasedKey} "1" -- "1" ${relationship.target.camelCasedKey}
    </#list>

    <#list element.hasZeroOrOnes as relationship>
        ${relationship.source.camelCasedKey} "1" -- "0..1" ${relationship.target.camelCasedKey}
    </#list>

    <#list element.hasZeroOrMores as relationship>
        ${relationship.source.camelCasedKey} "1" -- "*" ${relationship.target.camelCasedKey}
    </#list>

    <#list element.hasOneOrMores as relationship>
        ${relationship.source.camelCasedKey} "1" -- "1..*" ${relationship.target.camelCasedKey}
    </#list>

<#list element.entityTypes as entityType>
    hide <<${entityType.label}>> circle
</#list>

@enduml


