@startmindmap
!includeurl https://raw.githubusercontent.com/ebbypeter/Archimate-PlantUML/master/Archimate.puml

sprite $aActor jar:archimate/business-actor
sprite $aSystem jar:archimate/application-component

*[#BUSINESS]:  <$aActor> <b>${element.concept.title}</b>
<i>${element.concept.getType()}</i>

${element.concept.getDescriptionWrapped(60)};

<#list element.concept.getMembers() as member>
    **[#BUSINESS]: <$aActor> <b>${member.title}</b>
    <i>${member.getType()}</i>

    ${member.getDescriptionWrapped(60)};
</#list>

left side

<#list element.concept.getSystems() as sys>
    **[#APPLICATION]: <$aSystem> <b>${sys.title}</b>

    ${sys.getDescriptionWrapped(60)};
</#list>

@endmindmap