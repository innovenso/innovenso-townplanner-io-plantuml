@startuml
<#include "lib/archimate.ftl">
<#import  "lib/container.ftl" as c>
<#import  "lib/system.ftl" as s>
<#import  "lib/actor.ftl" as a>
<#import "lib/relationship.ftl" as r>


<#list element.centralSystems as system>
    <#if system.hasContainers()>
        System_Context(${system.key}, "${system.title}") {
        <#list system.containers as container>
            <@c.application_container container=container />
        </#list>
        }
    <#else>
        <@s.application_system system=system />
    </#if>
</#list>


<#-- render all the systems that interact with the system itself or any of the containers inside this system -->
    <#list element.systems as system>
        <@s.application_system system=system />
    </#list>

<#-- render all the actors that interact with the system itself or any of the containers inside this system -->
    <#list element.actors as actor>
        <@a.business_actor actor=actor />
    </#list>

<#-- render all relationships -->
    <#list element.flows as relationship>
        <@r.element_relationship relationship=relationship title=relationship.label />
    </#list>

@enduml