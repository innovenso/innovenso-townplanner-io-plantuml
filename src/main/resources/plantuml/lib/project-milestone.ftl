<#macro project_milestone milestone>
    ProjectMilestone(${milestone.key}, "${milestone.title?replace('"','')?replace('\n',' ')}", "${milestone.description?replace('"','')?replace('\n',' ')}")
</#macro>