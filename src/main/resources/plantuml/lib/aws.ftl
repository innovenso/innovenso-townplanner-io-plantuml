!define AWSURL https://raw.githubusercontent.com/plantuml/plantuml-stdlib/master/awslib
!includeurl AWSURL/AWSCommon.puml
!includeurl AWSURL/GroupIcons/all.puml
!includeurl AWSURL/NetworkingAndContentDelivery/all.puml
!includeurl AWSURL/Database/all.puml
!includeurl AWSURL/Compute/all.puml
!includeurl AWSURL/Storage/all.puml
!includeurl AWSURL/Analytics/all.puml
!includeurl AWSURL/ApplicationIntegration/all.puml
!includeurl AWSURL/ManagementAndGovernance/all.puml


!define DESC_FONT_SIZE 10
!define TECHN_FONT_SIZE 10
!define DESC_FONT_COLOR #CCCCCC

skinparam wrapWidth 250

skinparam rectangle<<region>> {
    Shadowing false
    StereotypeFontSize 0
    FontColor #444444
    BorderColor #444444
    BorderStyle dashed
}

skinparam rectangle<<vpc>> {
        Shadowing false
        StereotypeFontSize 0
        FontColor #444444
        BorderColor #444444
        BorderStyle solid
}

skinparam rectangle<<availability-zone>> {
    Shadowing false
    StereotypeFontSize 0
    FontColor #444444
    BorderColor #444444
    BorderStyle dotted
}

skinparam rectangle<<subnet>> {
    Shadowing false
    StereotypeFontSize 0
    FontColor #444444
    BorderColor #444444
    BorderStyle dashed
}

skinparam rectangle<<security-group>> {
    Shadowing false
    StereotypeFontSize 0
    FontColor #444444
    BorderColor #444444
    BorderStyle dashed
}

skinparam rectangle<<autoscaling-group>> {
    Shadowing false
    StereotypeFontSize 0
    FontColor #444444
    BorderColor #444444
    BorderStyle solid
}

skinparam rectangle<<route-table>> {
    Shadowing false
    StereotypeFontSize 0
    FontColor #444444
    BorderColor #444444
    BorderStyle solid
}

skinparam rectangle<<router>> {
    Shadowing false
    StereotypeFontSize 0
    FontColor #444444
    BorderColor #444444
    BorderStyle dotted
}

skinparam rectangle<<aws-instance>> {
    Shadowing false
    StereotypeFontSize 0
    FontColor #444444
    BorderColor #444444
    BorderStyle solid
}


!define AwsRegion(e_alias, e_label) rectangle "==e_label" <<$Region>><<region>> as e_alias
!define AwsVPC(e_alias, e_label, e_network, e_desc) rectangle "e_label\n\n<size:TECHN_FONT_SIZE>[e_network]</size>\n\n <size:DESC_FONT_SIZE>e_desc</size>" <<$VPC>><<vpc>> as e_alias
!define AwsAZ(e_alias, e_label) rectangle "==e_label" <<availability-zone>> as e_alias
!define AwsSubnet(e_alias, e_label, e_mask) rectangle "e_label\n\n<size:TECHN_FONT_SIZE>[e_mask]</size>" <<$VPCSubnetPublic>><<vpc>> as e_alias
!define AwsSecurityGroup(e_alias, e_label, e_desc) rectangle "e_label\n\n<size:TECHN_FONT_SIZE>[Security Group]</size>\n\n <size:DESC_FONT_SIZE>e_desc</size>" <<security-group>> as e_alias
!define AwsInstance(e_alias, e_label, e_instance_type, e_instance_logo, e_desc) rectangle "e_label\n\n<size:TECHN_FONT_SIZE>[e_instance_type]</size>\n\n <size:DESC_FONT_SIZE>e_desc</size>" <<$e_instance_logo>><<aws-instance>> as e_alias
!define AwsAutoScalingGroup(e_alias, e_label, e_desc) rectangle "e_label\n\n<size:TECHN_FONT_SIZE>[Auto Scaling Group]</size>\n\n <size:DESC_FONT_SIZE>e_desc</size>" <<$AutoScalingGroup>><<autoscaling-group>> as e_alias
!define AwsRouteTable(e_alias, e_label, e_desc) rectangle "e_label\n\n<size:TECHN_FONT_SIZE>[Route Table]</size>\n\n <size:DESC_FONT_SIZE>e_desc</size>" <<$Route53RouteTable>><<route-table>> as e_alias
!define AwsEip(e_alias, e_label, e_desc) rectangle "e_label\n\n<size:TECHN_FONT_SIZE>[Elastic IP Address]</size>\n\n <size:DESC_FONT_SIZE>e_desc</size>" <<$VPCElasticNetworkInterface>><<aws-instance>> as e_alias
!define AwsRouter(e_alias) rectangle "router" <<$VPCRouter>><<router>> as e_alias

