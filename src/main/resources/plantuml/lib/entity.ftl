<#import  "swot.ftl" as sw>
<#import  "lifecycle.ftl" as lc>

<#macro data_entity entity>
    class "${entity.title}" as ${entity.key} <<${entity.entityType.label}>>{
    <#list entity.fields as field>
        + ${field.title} : ${field.type} [${field.fieldConstraints}]
    </#list>
    }

    <@sw.concept_swot concept=entity />
    <@lc.concept_lifecycle concept=entity />
</#macro>

<#macro entity_group entityList title>
    <#if entityList?has_content>
        package "${title}" #ffffff {
        <#list entityList as entity>
            <@data_entity entity />
        </#list>
        }
    </#if>

</#macro>