<#import  "swot.ftl" as sw>
<#import  "lifecycle.ftl" as lc>
<#import "plantify.ftl" as plant>

<#macro application_system system>
    System(${system.key}, "${system.title?replace('"','')?replace('\n',' ')}", "${system.description?replace('"','')?replace('\n',' ')}", "${system.lifecycle.type.name()}")

    <@sw.concept_swot concept=system />
    <@lc.concept_lifecycle concept=system />
</#macro>

<#macro timemachine_system system conceptModel>
    <#if conceptModel.isAddedSinceLastKeyPointInTime(system)>
        System(${system.key}, "${system.title?replace('"','')?replace('\n',' ')}", "${system.description?replace('"','')?replace('\n',' ')}", "ADDED")
    <#elseif conceptModel.isRemovedSinceLastKeyPointInTime(system)>
        System(${system.key}, "${system.title?replace('"','')?replace('\n',' ')}", "${system.description?replace('"','')?replace('\n',' ')}", "DECOMMISSIONED")
    <#elseif conceptModel.isPhasingOut(system)>
        System(${system.key}, "${system.title?replace('"','')?replace('\n',' ')}", "${system.description?replace('"','')?replace('\n',' ')}", "PHASEOUT")
    <#else>
        System(${system.key}, "${system.title?replace('"','')?replace('\n',' ')}", "${system.description?replace('"','')?replace('\n',' ')}", "ACTIVE")
    </#if>
</#macro>

<#macro simple_system system>
    Application_Component(${system.key}, "${system.title}")
</#macro>