<#macro legend>
    legend
    |<back:DECOMMISSIONED><color:DECOMMISSIONED>X</color></back>| Removed since last point in time |
    |<back:ADDED><color:ADDED>X</color></back>| Added since last point in time |
    |<back:PHASEOUT><color:PHASEOUT>X</color></back>| Phaseout |
    endlegend
</#macro>