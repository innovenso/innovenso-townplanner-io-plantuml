<#import  "swot.ftl" as sw>
<#import  "lifecycle.ftl" as lc>

<#macro application_container container>
    Container(${container.key}, "${container.title?replace('"','')?replace('\n',' ')}", "${container.description?replace('"','')?replace('\n',' ')}", "${container.type}", "${container.technologyDescription?replace('"','')}", "${container.lifecycle.type.name()}")

    <@sw.concept_swot concept=container />
    <@lc.concept_lifecycle concept=container />
</#macro>

<#macro timemachine_container container conceptModel>
    <#if conceptModel.isAddedSinceLastKeyPointInTime(container)>
        Container(${container.key}, "${container.title?replace('"','')?replace('\n',' ')}", "${container.description?replace('"','')?replace('\n',' ')}", "${container.type}", "${container.technologyDescription?replace('"','')}", "ADDED")
    <#elseif conceptModel.isRemovedSinceLastKeyPointInTime(container)>
        Container(${container.key}, "${container.title?replace('"','')?replace('\n',' ')}", "${container.description?replace('"','')?replace('\n',' ')}", "${container.type}", "${container.technologyDescription?replace('"','')}", "DECOMMISSIONED")
    <#elseif conceptModel.isPhasingOut(container)>
        Container(${container.key}, "${container.title?replace('"','')?replace('\n',' ')}", "${container.description?replace('"','')?replace('\n',' ')}", "${container.type}", "${container.technologyDescription?replace('"','')}", "PHASEOUT")
    <#else>
        Container(${container.key}, "${container.title?replace('"','')?replace('\n',' ')}", "${container.description?replace('"','')?replace('\n',' ')}", "${container.type}", "${container.technologyDescription?replace('"','')}", "ACTIVE")
    </#if>
</#macro>
