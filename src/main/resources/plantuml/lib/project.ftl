<#macro project project>
    Project(${project.key}, "${project.title?replace('"','')?replace('\n',' ')}", "${project.description?replace('"','')?replace('\n',' ')}", "${project.type}")
</#macro>