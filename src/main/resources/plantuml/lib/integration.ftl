<#import  "swot.ftl" as sw>
<#import  "lifecycle.ftl" as lc>

<#macro system_integration integration>
    Integration(${integration.key}, "${integration.title?replace('"','')?replace('\n',' ')}", "${integration.description?replace('"','')?replace('\n',' ')}", "${integration.technologyDescription}", "${integration.lifecycle.type.name()}")

    <@sw.concept_swot concept=integration />
    <@lc.concept_lifecycle concept=integration />
</#macro>