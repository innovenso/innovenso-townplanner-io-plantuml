<#macro element_relationship relationship title>

    <#switch relationship.relationshipType.label>
        <#case "realization">
            Rel_Realization(${relationship.source.key}, ${relationship.target.key}, "${title!relationship.title?replace('"','')?replace('\n',' ')}")
            <#break>
        <#case "composition">
            Rel_Composition(${relationship.source.key}, ${relationship.target.key}, "${title!relationship.title?replace('"','')?replace('\n',' ')}")
            <#break>
        <#case "member">
            Rel_Association(${relationship.source.key}, ${relationship.target.key}, "${title!relationship.title?replace('"','')?replace('\n',' ')}")
            <#break>
        <#default>
            Rel_Flow(${relationship.source.key}, ${relationship.target.key}, "${title!relationship.label?replace('"','')?replace('\n',' ')}")
    </#switch>

    <#list relationship.swots as swot>
        SWOT(${swot.key}, ${swot.type.label}, "${swot.description?replace('"','')?replace('\n',' ')}")

        ${swot.key} --> ${relationship.target.key}
        ${swot.key} --> ${relationship.source.key}
    </#list>

</#macro>