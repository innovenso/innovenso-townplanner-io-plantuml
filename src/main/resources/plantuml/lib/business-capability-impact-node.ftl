<#macro business_capability_impact_node ic>
    <#local level=ic.capability.level!0>
    <#local indent=level+2>
    <#local title=ic.capability.title>
    <#local description=ic.capability.getDescriptionWrapped(60)>
    <#local color="RED">
    <#if !ic.impacted>
        <#if level gt 2>
            <#local color="BUSINESS">
        <#else>
            <#local color="STRATEGY">
        </#if>
    </#if>

        <#if level gt 2>
${""?left_pad(indent, "*")}[#${color}]: <$aFunction> <b>${title?replace('"','')?replace('\n',' ')}</b>

${description?replace('"','')};
        <#else>
${""?left_pad(indent, "*")}[#${color}]: <$aCapability> <b>${title?replace('"','')?replace('\n',' ')}</b>

${description?replace('"','')};
        </#if>

</#macro>