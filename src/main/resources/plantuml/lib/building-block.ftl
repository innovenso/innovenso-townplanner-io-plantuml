<#import  "swot.ftl" as sw>
<#import  "lifecycle.ftl" as lc>

<#macro application_building_block block>
    BuildingBlock(${block.key}, "${block.title?replace('"','')?replace('\n',' ')}", "${block.description?replace('"','')?replace('\n',' ')}", "${block.lifecycle.type.name()}")

    <@sw.concept_swot concept=block />
    <@lc.concept_lifecycle concept=block />
</#macro>