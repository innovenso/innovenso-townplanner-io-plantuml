<#macro business_capability capability>
    <#local level=capability.level!0>
    <#local title=capability.title>
    <#if capability.description?has_content>
        <#local title="${capability.title?replace('"','')?replace('\n',' ')}\\n\\n<i>${capability.description?replace('"','')?replace('\n',' ')}</i>\\n">
    </#if>
    <#if capability.hasChildren()!false>
        <#if level gt 2>
            Business_Function_Group(${capability.key}, "${title}"){
        <#else>
            Strategy_Capability_Group(${capability.key}, "${title}"){
        </#if>
        <#list capability.children as child>
            <@business_capability capability=child />
        </#list>
        }
    <#else>
        <#if level gt 2>
            Business_Function(${capability.key}, "${title}")
        <#else>
            Strategy_Capability(${capability.key}, "${title}")
        </#if>
    </#if>
</#macro>