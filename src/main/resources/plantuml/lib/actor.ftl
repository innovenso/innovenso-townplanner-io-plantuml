<#import  "swot.ftl" as sw>

<#macro business_actor actor>
    Business_Actor(${actor.key}, "${actor.title?replace('"','')?replace('\n',' ')}")

    <@sw.concept_swot concept=actor />
</#macro>

