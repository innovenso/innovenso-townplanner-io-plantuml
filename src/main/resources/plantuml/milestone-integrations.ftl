@startmindmap
!includeurl https://raw.githubusercontent.com/ebbypeter/Archimate-PlantUML/master/Archimate.puml

sprite $aInteraction jar:archimate/technology-interaction
sprite $aMilestone jar:archimate/implementation-plateau
sprite $aProject jar:archimate/implementation-workpackage

*[#IMPLEMENTATION]:  <$aProject> <b>${element.concept.project.title}</b>

${element.concept.project.getDescriptionWrapped(60)};

**[#IMPLEMENTATION]: <$aMilestone> <b>${element.title}</b>

${element.concept.getDescriptionWrapped(60)};

<#if element.concept.getAddedItSystemIntegrations()?has_content>
***[#GREEN] <&plus>
<#list element.concept.getAddedItSystemIntegrations() as cap>
    ****[#TECHNOLOGY]: <$aInteraction> <b>${cap.title}</b>

    ${cap.getDescriptionWrapped(60)};
</#list>
</#if>

<#if element.concept.getRemovedItSystemIntegrations()?has_content>
***[#RED] <&minus>
<#list element.concept.getRemovedItSystemIntegrations() as cap>
    ****[#TECHNOLOGY]: <$aInteraction> <b>${cap.title}</b>

    ${cap.getDescriptionWrapped(60)};
</#list>
</#if>

<#if element.concept.getChangedItSystemIntegrations()?has_content>
***[#YELLOW] <&pencil>
<#list element.concept.getChangedItSystemIntegrations() as cap>
    ****[#TECHNOLOGY]: <$aInteraction> <b>${cap.title}</b>

    ${cap.getDescriptionWrapped(60)};
</#list>
</#if>

@endmindmap