@startuml
<#include "lib/archimate.ftl">

<#list view.systemContexts as system>
System_Context(${system.key}, "${system.title}") {
<#list view.getContainers(system.key) as container>
    Container(${container.key}, "${container.title}", "${container.description}", "${container.type}", "${container.technologyDescription}", "${container.lifecycle.type.name()}")
</#list>
}
</#list>

<#list view.systems as system>
    System(${system.key}, "${system.title}", "${system.description}", "${system.lifecycle.type.name()}")
</#list>

<#list view.actors as actor>
    Business_Actor(${actor.key}, "${actor.title}")
</#list>

<#list view.steps as step>
    <#if view.view.showStepCounter>
        <#if step.response>
            Rel_Flow(${step.relationship.target.key}, ${step.relationship.source.key}, "${step?counter}. ${step.label}")
        <#else>
            Rel_Flow(${step.relationship.source.key}, ${step.relationship.target.key}, "${step?counter}. ${step.label}")
        </#if>
    <#else>
        <#if step.response>
            Rel_Flow(${step.relationship.target.key}, ${step.relationship.source.key}, "${step.label}")
        <#else>
            Rel_Flow(${step.relationship.source.key}, ${step.relationship.target.key}, "${step.label}")
        </#if>
    </#if>
</#list>

@enduml