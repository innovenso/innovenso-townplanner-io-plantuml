@startmindmap
!includeurl https://raw.githubusercontent.com/ebbypeter/Archimate-PlantUML/master/Archimate.puml

sprite $aCapability jar:archimate/strategy-capability
sprite $aFunction jar:archimate/business-function
sprite $aMilestone jar:archimate/implementation-plateau
sprite $aProject jar:archimate/implementation-workpackage

*[#IMPLEMENTATION]  <$aProject> <b>${element.project.title}</b>

**[#IMPLEMENTATION]: <$aMilestone> <b>${element.title}</b>

${element.concept.getDescriptionWrapped(60)};

<#if element.addedTree?has_content>
***[#GREEN] <&plus>
<#list element.addedTree as capability>
${""?left_pad(capability.level + 4, "*")}[#STRATEGY] <$aCapability> <b>${capability.title}</b>
</#list>
</#if>

<#if element.removedTree?has_content>
***[#RED] <&minus>
<#list element.removedTree as capability>
${""?left_pad(capability.level + 4, "*")}[#STRATEGY] <$aCapability> <b>${capability.title}</b>
</#list>
</#if>

<#if element.changedTree?has_content>
***[#YELLOW] <&pencil>
<#list element.changedTree as capability>
${""?left_pad(capability.level + 4, "*")}[#STRATEGY] <$aCapability> <b>${capability.title}</b>
</#list>
</#if>

@endmindmap