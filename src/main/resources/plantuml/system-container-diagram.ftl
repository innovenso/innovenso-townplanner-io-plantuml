@startuml
<#include "lib/archimate.ftl">
<#import  "lib/container.ftl" as c>
<#import  "lib/system.ftl" as s>
<#import  "lib/actor.ftl" as a>
<#import "lib/relationship.ftl" as r>
<#import "lib/timemachinelegend.ftl" as time>

<#if element.hasContainers()>
    System_Context(${element.centralSystem.key}, "${element.centralSystem.title}") {
    <#list element.containers as container>
        <@c.timemachine_container container=container conceptModel=element />
    </#list>
    }
<#else>
    <@s.timemachine_system system=element.centralSystem conceptModel=element />
</#if>

<#-- render all the systems that interact with the system itself or any of the containers inside this system -->
    <#list element.systems as system>
        <@s.timemachine_system system=system conceptModel=element />
    </#list>

<#-- render all the actors that interact with the system itself or any of the containers inside this system -->
    <#list element.actors as actor>
        <@a.business_actor actor=actor />
    </#list>

<#-- render all relationships -->
    <#list element.relationships as relationship>
        <@r.element_relationship relationship=relationship title=relationship.label />
    </#list>

<@time.legend />
@enduml