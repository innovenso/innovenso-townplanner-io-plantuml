@startuml
<#include "lib/archimate.ftl">
<#import  "lib/system.ftl" as s>
<#import  "lib/integration.ftl" as i>

<@s.application_system system=element.concept />

<#list element.concept.integrations as integration>
    <@i.system_integration integration=integration />
    Rel_Association(${integration.key}, ${element.key}, "")

    <#list integration.systems as system>
        <#if system.key != element.key>
            <@s.application_system system=system />
            Rel_Association(${system.key}, ${integration.key}, "")
        </#if>
    </#list>
</#list>

@enduml