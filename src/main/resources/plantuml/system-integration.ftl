@startuml
<#include "lib/archimate.ftl">
<#import  "lib/system.ftl" as s>
<#import  "lib/integration.ftl" as i>
<#import  "lib/entity.ftl" as e>

<@i.system_integration integration=element.concept />

<@s.application_system system=element.concept.sourceSystem />
${element.concept.sourceSystem.key} -l- ${element.key}

<@s.application_system system=element.concept.targetSystem />
${element.concept.targetSystem.key} -r- ${element.key}

<#list element.concept.implementingSystems as implementor>
    <@s.application_system system=implementor />
    Rel_Realization_Up(${implementor.key}, ${element.key}, "")
</#list>

<#list element.concept.dataEntities as entity>
    <@e.data_entity entity=entity />
    Rel_Composition_Down(${entity.key}, ${element.key}, "")
</#list>

@enduml