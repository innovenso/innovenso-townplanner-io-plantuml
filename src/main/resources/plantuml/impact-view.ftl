@startmindmap
!includeurl https://raw.githubusercontent.com/ebbypeter/Archimate-PlantUML/master/Archimate.puml

sprite $aCapability jar:archimate/strategy-capability
sprite $aFunction jar:archimate/business-function
sprite $aEnterprise jar:archimate/physical-facility

<#import "lib/business-capability-impact-node.ftl" as cap>

<#list view.enterprises as enterprise>
*[#PHYSICAL]  <$aEnterprise> ${enterprise.title}

    <#list view.getBusinessCapabilities(enterprise) as ic>
<@cap.business_capability_impact_node ic=ic />
    </#list>
</#list>


@endmindmap