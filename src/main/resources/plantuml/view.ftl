@startuml
!includeurl https://raw.githubusercontent.com/ebbypeter/Archimate-PlantUML/master/Archimate.puml

<#list element.businessCapabilities as capability>
Strategy_Capability(${capability.label?replace("-","_")}, "${capability.title}")
</#list>

<#list element.buildingBlocks as buildingBlock>
Business_Service(${buildingBlock.label?replace("-","_")}, "${buildingBlock.title}")
</#list>

<#list element.capabilityBuildingBlockMappings as connection>
Rel_Realization(${connection.buildingBlock?replace("-", "_")}, ${connection.capability?replace("-", "_")}, "${connection.title}")
</#list>

<#list element.systems as system>
Application_Component(${system.label?replace("-","_")}, "${system.title}")
</#list>

<#list element.buildingBlockSystemMappings as connection>
Rel_Realization(${connection.system?replace("-", "_")}, ${connection.buildingBlock?replace("-", "_")}, "${connection.title}")
</#list>

<#list element.containers as container>
Application_Component(${container.label?replace("-","_")}, "${container.title}")
</#list>

<#list element.itConnections as connection>
Rel_Flow(${connection.source?replace("-", "_")}, ${connection.target?replace("-", "_")}, "${connection.title}")
</#list>

<#list element.actors as actor>
Business_Actor(${actor.label?replace("-", "_")}, "${actor.title}")
</#list>

<#list element.actorConnections as connection>
Rel_Triggering(${connection.actor?replace("-", "_")}, ${connection.target?replace("-", "_")}, "${connection.title}")
</#list>

<#list element.entities as entity>
Application_DataObject(${entity.label?replace("-", "_")}, "${entity.title}")
</#list>
@enduml