@startmindmap
!includeurl https://raw.githubusercontent.com/ebbypeter/Archimate-PlantUML/master/Archimate.puml

sprite $aEntity jar:archimate/application-data-object
sprite $aMilestone jar:archimate/implementation-plateau
sprite $aProject jar:archimate/implementation-workpackage

*[#IMPLEMENTATION]:  <$aProject> <b>${element.concept.project.title}</b>

${element.concept.project.getDescriptionWrapped(60)};

**[#IMPLEMENTATION]: <$aMilestone> <b>${element.title}</b>

${element.concept.getDescriptionWrapped(60)};

<#if element.concept.getAddedDataEntities()?has_content>
***[#GREEN] <&plus>
<#list element.concept.getAddedDataEntities() as cap>
    ****[#APPLICATION]: <$aEntity> <b>${cap.title}</b>

    ${cap.getDescriptionWrapped(60)};
</#list>
</#if>

<#if element.concept.getRemovedDataEntities()?has_content>
***[#RED] <&minus>
<#list element.concept.getRemovedDataEntities() as cap>
    ****[#APPLICATION]: <$aEntity> <b>${cap.title}</b>

    ${cap.getDescriptionWrapped(60)};
</#list>
</#if>

<#if element.concept.getChangedDataEntities()?has_content>
***[#YELLOW] <&pencil>
<#list element.concept.getChangedDataEntities() as cap>
    ****[#APPLICATION]: <$aEntity> <b>${cap.title}</b>

    ${cap.getDescriptionWrapped(60)};
</#list>
</#if>

@endmindmap