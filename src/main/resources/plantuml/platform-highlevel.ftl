@startuml
<#include "lib/archimate.ftl">
<#import  "lib/system.ftl" as s>
<#import  "lib/actor.ftl" as a>
<#import "lib/relationship.ftl" as r>
<#import "lib/timemachinelegend.ftl" as time>


Platform(${element.platform.key}, "${element.platform.title}") {
    <#list element.innerSystems as system>
        <@s.timemachine_system system=system conceptModel=element />
    </#list>
}

    <#list element.outerSystems as system>
        <@s.timemachine_system system=system conceptModel=element />
    </#list>

    <#list element.actors as actor>
        <@a.business_actor actor=actor />
    </#list>

    <#list element.flows as relationship>
        <@r.element_relationship relationship=relationship title=relationship.label />
    </#list>

<@time.legend />
@enduml