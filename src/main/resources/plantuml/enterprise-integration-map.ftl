@startuml
<#include "lib/archimate.ftl">
<#import  "lib/system.ftl" as s>
<#import "lib/relationship.ftl" as r>
<#import "lib/timemachinelegend.ftl" as time>

<#list element.systems as system>
    <@s.timemachine_system system=system conceptModel=element />
</#list>

<#list element.systemIntegrationRelationships as relationship>
    Rel_Access(${relationship.source.key}, ${relationship.target.key}, "${relationship.label?replace('"','')?replace('\n',' ')}")
</#list>

<@time.legend />

@enduml