@startuml
<#include "lib/archimate.ftl">

<#list element.systemContexts as system>
System_Context(${system.key}, "${system.title}") {
<#list element.getContainers(system.key) as container>
    Container(${container.key}, "${container.title}", "${container.description}", "${container.type}", "${container.technologyDescription}", "${container.lifecycle.type.name()}")
</#list>
}
</#list>

<#list element.systems as system>
    System(${system.key}, "${system.title}", "${system.description}", "${system.lifecycle.type.name()}")
</#list>

<#list element.actors as actor>
    Business_Actor(${actor.key}, "${actor.title}")
</#list>

<#list element.steps as step>
      <#if step.response>
            Rel_Flow(${step.relationship.target.key}, ${step.relationship.source.key}, "${step?counter}. ${step.label}")
        <#else>
            Rel_Flow(${step.relationship.source.key}, ${step.relationship.target.key}, "${step?counter}. ${step.label}")
        </#if>
</#list>


@enduml