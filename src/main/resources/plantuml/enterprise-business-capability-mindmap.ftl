@startmindmap
!includeurl https://raw.githubusercontent.com/ebbypeter/Archimate-PlantUML/master/Archimate.puml

sprite $aCapability jar:archimate/strategy-capability
sprite $aFunction jar:archimate/business-function
sprite $aEnterprise jar:archimate/physical-facility

*[#PHYSICAL]  <$aEnterprise> ${element.title}
<#list element.businessCapabilities as capability>
<#if capability.level gt 2>
${""?left_pad(capability.level + 2, "*")}[#BUSINESS]: <$aFunction> <b>${capability.title}</b>

${capability.getDescriptionWrapped(60)};
<#else>
${""?left_pad(capability.level + 2, "*")}[#STRATEGY]: <$aCapability> <b>${capability.title}</b>

${capability.getDescriptionWrapped(60)};
</#if>
</#list>

@endmindmap