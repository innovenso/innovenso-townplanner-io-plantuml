@startuml
<#include "lib/aws.ftl">

<#list element.regions as region>
    AwsRegion(${region.key}, "${region.title}") {
    <#list region.vpcs as vpc>
        AwsVPC(${vpc.key}, "${vpc.title}", "${vpc.networkMask}", "${vpc.description}") {
        <#list vpc.availabilityZones as az>
            AwsAZ(${az.key}, "${az.title}") {
            <#list element.getSubnets(vpc.key, az.key) as subnet>
                AwsSubnet(${subnet.key}, "${subnet.title}", "${subnet.subnetMask}") {
                <#list element.getNonEmptySecurityGroups(vpc.key, subnet.key) as securityGroup>
                    AwsSecurityGroup(${securityGroup.key}, "${securityGroup.title}") {
                       <#list securityGroup.instances as instance>
                           AwsInstance(${instance.key}, "${instance.title}", "${instance.instanceType}", "${instance.description}")
                       </#list>
                    }
                </#list>
                }
            </#list>
            }
        </#list>
        }
    </#list>
    <#list element.getRegionalInstances(region.key) as instance>
        AwsInstance(${instance.key}, "${instance.title}", "${instance.instanceType}", "${instance.description}")
    </#list>
    }
</#list>
<#list element.getGlobalInstances() as instance>
    AwsInstance(${instance.key}, "${instance.title}", "${instance.instanceType}", "${instance.description}")
</#list>


@enduml